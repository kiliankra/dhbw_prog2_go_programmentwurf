package webserver

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	cfg "prog2_go_programmentwurf/src/config"
	"prog2_go_programmentwurf/src/locations"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

// tokens were generated with https://jwt.io

func getTestConfig() cfg.Config {
	// create temp file for secret
	file, err := ioutil.TempFile("", "*.txt")
	if err != nil {
		panic("could not create temp file")
	}
	// Stacked defer calls -> executed in reverse order
	// File can be deleted after the config has been generated
	defer func() { _ = os.Remove(file.Name()) }()
	defer func() { _ = file.Close() }()

	secret := "mySecret"
	_, err = file.WriteString(secret)
	if err != nil {
		panic("could not write to file")
	}

	// create temp file for locations
	fileLocations, err := ioutil.TempFile("", "*.xml")
	if err != nil {
		panic("could not create temp file")
	}
	// Stacked defer calls -> executed in reverse order
	// File can be deleted after the config has been generated
	defer func() { _ = os.Remove(fileLocations.Name()) }()
	defer func() { _ = fileLocations.Close() }()

	_, err = fileLocations.WriteString(
		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
			"<locations>\n" +
			"    <location>\n" +
			"        <id>1</id>\n" +
			"        <name>DHBW Mosbach, B-Gebäude</name>\n" +
			"    </location>\n" +
			"</locations>",
	)
	if err != nil {
		panic("could not write to file")
	}

	args := make([]string, 4)
	args[0] = "-tokensecretpath"
	args[1] = file.Name()
	args[2] = "-locationxmlpath"
	args[3] = fileLocations.Name()

	var config cfg.Config = cfg.NewConfig(args)
	return config
}

var testConfig = getTestConfig()

func makeUserTokenTest(t *testing.T, handler http.Handler, token string) (*httptest.ResponseRecorder, []byte) {
	req, err := http.NewRequest("GET", fmt.Sprintf("/api%v?token=%v", testConfig.LoginSitePath(), token), nil)
	assert.NoError(t, err)
	assert.NotNil(t, req)

	req.Form = make(url.Values)
	req.Form.Add("name", "Max Mustermann")
	req.Form.Add("address", "Musterstraße 42, 12345 Musterstadt")

	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	body, err := ioutil.ReadAll(rr.Body)
	assert.NoError(t, err)

	return rr, body
}

func TestIsTokenExpired(t *testing.T) {
	var validity int64 = 1000 * 60
	now := time.Now().UnixMilli()

	payload := locations.LoginTokenPayload{
		CreatedAt:  now,
		ExpiresAt:  now + validity,
		LocationId: 1,
	}

	assert.False(t, isTokenExpired(payload, validity))

	// payload that is expired for 1 second
	payload = locations.LoginTokenPayload{
		CreatedAt:  now - 2*validity,
		ExpiresAt:  now - validity - 1,
		LocationId: 1,
	}
	assert.True(t, isTokenExpired(payload, validity))
}

func TestUserTokenWrapperWhenJwtInvalid(t *testing.T) {
	handler := UserTokenWrapper(func(w http.ResponseWriter, r *http.Request) {
		t.Fail()
	}, &testConfig)

	rr, body := makeUserTokenTest(t, handler, "invalidToken")

	assert.Equal(t, http.StatusUnauthorized, rr.Code)
	assert.Contains(t, string(body), "anmeldetoken ist ungültig, ")
}

func TestUserTokenWrapperWhenTokenIsExpired(t *testing.T) {
	handler := UserTokenWrapper(func(w http.ResponseWriter, r *http.Request) {
		t.Fail()
	}, &testConfig)

	// has paylod createdAt: 100 and expiresAt: 200
	expiredToken := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjcmVhdGVkQXQiOjEwMCwiZXhwaXJlc0F0IjoyMDAsImxvY2F0aW9uSWQiOjF9.A8WLN9gk10XPpJL3v25r5ueqjxj7avTNkfC0u2ADjiI"

	rr, body := makeUserTokenTest(t, handler, expiredToken)

	assert.Equal(t, http.StatusUnauthorized, rr.Code)
	assert.Equal(t, "anmeldetoken ist abgelaufen\n", string(body))
}

func TestUserTokenWrapperWhenTokenIsValid(t *testing.T) {
	handlerCalled := false

	handler := UserTokenWrapper(func(w http.ResponseWriter, r *http.Request) {
		handlerCalled = true
		_, err := w.Write([]byte("Testbody"))
		assert.NoError(t, err)
	}, &testConfig)

	// token payload:
	// createdAt: 99900000000000 (year 5135)
	// expiresAt: 99999999999999 (year 5138)
	// locationId: 1
	const validToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjcmVhdGVkQXQiOjk5OTAwMDAwMDAwMDAwLCJleHBpcmVzQXQiOjk5OTk5OTk5OTk5OTk5LCJsb2NhdGlvbklkIjoxfQ.7aOxLq6Trn4QudnjNo9V3_ggQlykznIA6cL_MDL1BkQ"

	rr, body := makeUserTokenTest(t, handler, validToken)

	assert.Equal(t, http.StatusOK, rr.Code)
	assert.True(t, handlerCalled)
	assert.Equal(t, "Testbody", string(body))
}
