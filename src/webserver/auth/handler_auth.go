/*
 * 2627585, 3110300, 2858031
 */

package auth

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"prog2_go_programmentwurf/src/auth"
	"prog2_go_programmentwurf/src/config"
	"prog2_go_programmentwurf/src/webserver"
	"time"
)

type AuthHandler struct {
	cfg *config.Config
}

type LoginPageData struct {
	LoginApiUrl string
	LogoutUrl   string
	UserName    string
	UserAddress string
}

type LogoutPageData struct {
	LocationName string
	LogoutApiUrl string
}

type FormPrefillDataCookiePayload struct {
	Username string `json:"username"`
	Address  string `json:"address"`
}

const UserTokenCookieName = "dhbwGoUserToken"
const FormPrefillDataCookieName = "dhbwGoFormPrefillData"

var templateLogin = webserver.GetTemplate("login.html")
var templateLogout = webserver.GetTemplate("logout.html")

// Handler to serve the HTML code for the login page
func (authHandler AuthHandler) ServeLoginPage(w http.ResponseWriter, r *http.Request) {
	// Check if the user is already logged in -> if yes: redirect to the logout page
	_, err := r.Cookie(UserTokenCookieName)
	if err == nil {
		http.Redirect(w, r, fmt.Sprintf("https://%v:%v%v", (*authHandler.cfg).ServerHostAddress(), fmt.Sprint((*authHandler.cfg).LoginSitePort()), (*authHandler.cfg).LogoutSitePath()), http.StatusSeeOther)
	}

	// Prefill name and address if the user already logged in before
	prefilledUserName := ""
	prefilledUserAddress := ""
	cookieFormPrefillData, err := r.Cookie(FormPrefillDataCookieName)
	if err == nil {
		cookiePayloadJson, err := base64.StdEncoding.DecodeString(cookieFormPrefillData.Value)

		var cookiePayload FormPrefillDataCookiePayload
		err = json.Unmarshal(cookiePayloadJson, &cookiePayload)
		if err == nil {
			prefilledUserName = cookiePayload.Username
			prefilledUserAddress = cookiePayload.Address
		}
	}

	// Aggregate data for the login page
	loginPageData := LoginPageData{
		fmt.Sprintf("https://%v:%v/api%v?token=%v", (*authHandler.cfg).ServerHostAddress(), fmt.Sprint((*authHandler.cfg).LoginSitePort()), (*authHandler.cfg).LoginSitePath(), r.URL.Query().Get("token")),
		fmt.Sprintf("https://%v:%v%v", (*authHandler.cfg).ServerHostAddress(), fmt.Sprint((*authHandler.cfg).LoginSitePort()), (*authHandler.cfg).LogoutSitePath()),
		prefilledUserName,
		prefilledUserAddress,
	}

	// Parse the login page template and write the result to the response writer
	webserver.LogParseTemplateError(templateLogin.ExecuteTemplate(w, "login.html", loginPageData))
}

// Handler to perform the login
func (authHandler AuthHandler) ServeLogin(w http.ResponseWriter, r *http.Request) {
	// Get location id from the request context
	locationId, ok := r.Context().Value(webserver.LocationIdContextKey).(int64)
	if !ok {
		http.Error(w, "location id fehlt im Kontext oder ist kein int64", http.StatusInternalServerError)
		return
	}

	// Perform the login
	name := r.FormValue("name")
	address := r.FormValue("address")
	userToken, inputOk, err := auth.Login(locationId, name, address, authHandler.cfg)
	if err != nil {
		code := http.StatusInternalServerError
		if !inputOk {
			code = http.StatusBadRequest
		}
		http.Error(w, err.Error(), code)
		return
	}

	// Set the user token cookie
	cookieUserToken := http.Cookie{Name: UserTokenCookieName, Value: userToken, Path: "/"}
	http.SetCookie(w, &cookieUserToken)

	// Set the cookie for prefilling name and address next time
	formPrefillDataCookiePayload := FormPrefillDataCookiePayload{
		Username: name,
		Address:  address,
	}
	marshalledFormPrefillDataCookiePayload, err := json.Marshal(formPrefillDataCookiePayload)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	cookieFormPrefillData := http.Cookie{Name: FormPrefillDataCookieName, Value: base64.StdEncoding.EncodeToString(marshalledFormPrefillDataCookiePayload), Path: "/"}
	http.SetCookie(w, &cookieFormPrefillData)

	webserver.LogResponseWriteError(w.Write([]byte("")))
}

// Handler to serve the HTML code for the logout page
func (authHandler AuthHandler) ServeLogoutPage(w http.ResponseWriter, r *http.Request) {
	// Check if user is logged in
	cookieUserToken, err := r.Cookie(UserTokenCookieName)
	if err != nil {
		http.Error(w, "keine aktive anmeldung gefunden", http.StatusBadRequest)
		return
	}

	// Get location name from user token
	userTokenPayload, err := auth.CheckUserToken(cookieUserToken.Value, (*authHandler.cfg).TokenSignatureSecret())
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	location, err := (*authHandler.cfg).GetLocationById(userTokenPayload.LocationId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Aggregate data for the logout page
	logoutPageData := LogoutPageData{
		LocationName: location.Name,
		LogoutApiUrl: fmt.Sprintf("https://%v:%v/api%v", (*authHandler.cfg).ServerHostAddress(), fmt.Sprint((*authHandler.cfg).LoginSitePort()), (*authHandler.cfg).LogoutSitePath()),
	}

	// Parse the logout page template and write the result to the response writer
	webserver.LogParseTemplateError(templateLogout.ExecuteTemplate(w, "logout.html", logoutPageData))
}

// Handler to perform the logout
func (authHandler AuthHandler) ServeLogout(w http.ResponseWriter, r *http.Request) {
	// Check if user is logged in
	cookieUserToken, err := r.Cookie(UserTokenCookieName)
	if err != nil {
		http.Error(w, "keine aktive anmeldung gefunden", http.StatusBadRequest)
		return
	}

	// Perform the logout
	err = auth.Logout(cookieUserToken.Value, authHandler.cfg)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Deleting UserToken Cookie with setting expire date to the past
	expire := time.Now().Add(-1 * time.Hour)
	cookie := http.Cookie{
		Name:    UserTokenCookieName,
		Value:   "",
		Path:    "/",
		Expires: expire,
	}
	http.SetCookie(w, &cookie)

	webserver.LogResponseWriteError(w.Write([]byte("")))
}
