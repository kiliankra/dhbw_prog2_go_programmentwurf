/*
 * 2627585, 3110300, 2858031
 */

package auth

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"prog2_go_programmentwurf/src/auth"
	cfg "prog2_go_programmentwurf/src/config"
	"prog2_go_programmentwurf/src/locations"
	"prog2_go_programmentwurf/src/webserver"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

// Test for serving the login page where UserToken Cookie is set
func TestServeLoginPageUserTokenCookie(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.NoError(t, err)

	loginToken, err := locations.GenerateNewLoginToken(&testConfig, 1)
	assert.NoError(t, err)

	req, err := http.NewRequest("GET", fmt.Sprintf("%v?token=%v", testConfig.LoginSitePath(), loginToken), nil)
	assert.NoError(t, err)
	assert.NotNil(t, req)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(AuthHandler{&testConfig}.ServeLoginPage)

	cookieUserToken := http.Cookie{Name: UserTokenCookieName, Value: "dummy", Path: "/"}
	req.AddCookie(&cookieUserToken)

	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusSeeOther, rr.Code)
}

// Successful test for serving the login page
func TestServeLoginPage(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.NoError(t, err)

	loginToken, err := locations.GenerateNewLoginToken(&testConfig, 1)
	assert.NoError(t, err)

	req, err := http.NewRequest("GET", fmt.Sprintf("%v?token=%v", testConfig.LoginSitePath(), loginToken), nil)
	assert.NoError(t, err)
	assert.NotNil(t, req)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(AuthHandler{&testConfig}.ServeLoginPage)

	formPrefillDataCookiePayload := FormPrefillDataCookiePayload{
		Username: "Helmut Neemann",
		Address:  "Musterstraße 42, 12345 Musterstadt",
	}
	marshalledFormPrefillDataCookiePayload, err := json.Marshal(formPrefillDataCookiePayload)
	assert.NoError(t, err)
	cookieFormPrefillData := http.Cookie{Name: FormPrefillDataCookieName, Value: base64.StdEncoding.EncodeToString(marshalledFormPrefillDataCookiePayload), Path: "/"}
	req.AddCookie(&cookieFormPrefillData)

	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusOK, rr.Code)
}

// Test for processing login where login token is missing
func TestServeLoginNoLocationId(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.NoError(t, err)

	req, err := http.NewRequest("GET", testConfig.LoginSitePath(), nil)
	assert.NoError(t, err)
	assert.NotNil(t, req)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(AuthHandler{&testConfig}.ServeLogin)
	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusInternalServerError, rr.Code)

	body, err := ioutil.ReadAll(rr.Body)
	assert.NoError(t, err)
	assert.Equal(t, "location id fehlt im Kontext oder ist kein int64\n", string(body))
}

// Test for processing login where name is missing
func TestServeLoginNoName(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.NoError(t, err)

	req, err := http.NewRequest("GET", testConfig.LoginSitePath(), nil)
	assert.NoError(t, err)
	assert.NotNil(t, req)

	var locationId int64 = 1
	ctx := context.WithValue(req.Context(), webserver.LocationIdContextKey, locationId)
	assert.NotNil(t, ctx)
	req = req.WithContext(ctx)
	assert.NotNil(t, req)

	req.Form = make(url.Values)
	req.Form.Add("address", "Musterstraße 42, 12345 Musterstadt")

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(AuthHandler{&testConfig}.ServeLogin)
	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusBadRequest, rr.Code)

	body, err := ioutil.ReadAll(rr.Body)
	assert.NoError(t, err)
	assert.Equal(t, "anmeldung nicht möglich, da der Name leer ist\n", string(body))
}

// Test for processing login where address is missing
func TestServeLoginNoAddress(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.NoError(t, err)

	req, err := http.NewRequest("GET", testConfig.LoginSitePath(), nil)
	assert.NoError(t, err)
	assert.NotNil(t, req)

	var locationId int64 = 1
	ctx := context.WithValue(req.Context(), webserver.LocationIdContextKey, locationId)
	assert.NotNil(t, ctx)
	req = req.WithContext(ctx)
	assert.NotNil(t, req)

	req.Form = make(url.Values)
	req.Form.Add("name", "Helmut Neemann")

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(AuthHandler{&testConfig}.ServeLogin)
	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusBadRequest, rr.Code)

	body, err := ioutil.ReadAll(rr.Body)
	assert.NoError(t, err)
	assert.Equal(t, "anmeldung nicht möglich, da die Adresse leer ist\n", string(body))
}

// Successful test for processing login
func TestServeLogin(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.NoError(t, err)

	req, err := http.NewRequest("GET", testConfig.LoginSitePath(), nil)
	assert.NoError(t, err)
	assert.NotNil(t, req)

	var locationId int64 = 1
	ctx := context.WithValue(req.Context(), webserver.LocationIdContextKey, locationId)
	assert.NotNil(t, ctx)
	req = req.WithContext(ctx)
	assert.NotNil(t, req)

	req.Form = make(url.Values)
	req.Form.Add("name", "Helmut Neemann")
	req.Form.Add("address", "Musterstraße 42, 12345 Musterstadt")

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(AuthHandler{&testConfig}.ServeLogin)
	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusOK, rr.Code)

	body, err := ioutil.ReadAll(rr.Body)
	assert.NoError(t, err)
	assert.Equal(t, "", string(body))

	cookiesCount := len(rr.Result().Cookies())
	assert.Equal(t, 2, cookiesCount)

	cookieUserToken := rr.Result().Cookies()[0]
	assert.NotNil(t, cookieUserToken)
	assert.Equal(t, UserTokenCookieName, cookieUserToken.Name)
	assert.Equal(t, "/", cookieUserToken.Path)

	cookieFormPrefillData := rr.Result().Cookies()[1]
	assert.NotNil(t, cookieFormPrefillData)
	assert.Equal(t, FormPrefillDataCookieName, cookieFormPrefillData.Name)
	assert.Equal(t, "/", cookieFormPrefillData.Path)
}

// Test for serving the logout page where UserToken Cookie is missing
func TestServeLogoutPageNoUserTokenCookie(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.NoError(t, err)

	req, err := http.NewRequest("GET", testConfig.LogoutSitePath(), nil)
	assert.NoError(t, err)
	assert.NotNil(t, req)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(AuthHandler{&testConfig}.ServeLogoutPage)
	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusBadRequest, rr.Code)

	body, err := ioutil.ReadAll(rr.Body)
	assert.NoError(t, err)
	assert.Equal(t, "keine aktive anmeldung gefunden\n", string(body))
}

// Test for serving the logout page where UserToken is invalid
func TestServeLogoutPageUserTokenInvalid(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.NoError(t, err)

	req, err := http.NewRequest("GET", testConfig.LogoutSitePath(), nil)
	assert.NoError(t, err)
	assert.NotNil(t, req)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(AuthHandler{&testConfig}.ServeLogoutPage)

	cookieUserToken := http.Cookie{Name: UserTokenCookieName, Value: "Invalid Cookie Value", Path: "/"}
	req.AddCookie(&cookieUserToken)

	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusInternalServerError, rr.Code)

	body, err := ioutil.ReadAll(rr.Body)
	assert.NoError(t, err)
	assert.Equal(t, "benutzertoken ist ungültig, token format ungültig, er enthält keinen header, payload und Signatur. Token: Invalid Cookie Value\n", string(body))
}

// Successful test for serving the logout page
func TestServeLogoutPage(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.NoError(t, err)

	req, err := http.NewRequest("GET", testConfig.LogoutSitePath(), nil)
	assert.NoError(t, err)
	assert.NotNil(t, req)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(AuthHandler{&testConfig}.ServeLogoutPage)

	userToken, inputOk, err := auth.Login(1, "Helmut Neemann", "Musterstraße 42, 12345 Musterstadt", &testConfig)
	assert.NoError(t, err)
	assert.True(t, inputOk)
	assert.NotNil(t, userToken)
	cookieUserToken := http.Cookie{Name: UserTokenCookieName, Value: userToken, Path: "/"}
	req.AddCookie(&cookieUserToken)

	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusOK, rr.Code)
}

// Test for serving the logout where UserToken Cookie is missing
func TestServeLogoutNoUserTokenCookie(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.NoError(t, err)

	req, err := http.NewRequest("GET", fmt.Sprintf("/api%v", testConfig.LogoutSitePath()), nil)
	assert.NoError(t, err)
	assert.NotNil(t, req)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(AuthHandler{&testConfig}.ServeLogout)
	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusBadRequest, rr.Code)

	body, err := ioutil.ReadAll(rr.Body)
	assert.NoError(t, err)
	assert.Equal(t, "keine aktive anmeldung gefunden\n", string(body))
}

// Test for serving the logout where UserToken is invalid
func TestServeLogoutUserTokenInvalid(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.NoError(t, err)

	req, err := http.NewRequest("GET", fmt.Sprintf("/api%v", testConfig.LogoutSitePath()), nil)
	assert.NoError(t, err)
	assert.NotNil(t, req)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(AuthHandler{&testConfig}.ServeLogout)

	cookieUserToken := http.Cookie{Name: UserTokenCookieName, Value: "Invalid Cookie Value", Path: "/"}
	req.AddCookie(&cookieUserToken)

	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusInternalServerError, rr.Code)

	body, err := ioutil.ReadAll(rr.Body)
	assert.NoError(t, err)
	assert.Equal(t, "benutzertoken ist ungültig, token format ungültig, er enthält keinen header, payload und Signatur. Token: Invalid Cookie Value\n", string(body))
}

// Successful test for serving the logout
func TestServeLogout(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.NoError(t, err)

	req, err := http.NewRequest("GET", fmt.Sprintf("/api%v", testConfig.LogoutSitePath()), nil)
	assert.NoError(t, err)
	assert.NotNil(t, req)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(AuthHandler{&testConfig}.ServeLogout)

	userToken, inputOk, err := auth.Login(1, "Helmut Neemann", "Musterstraße 42, 12345 Musterstadt", &testConfig)
	assert.NoError(t, err)
	assert.True(t, inputOk)
	assert.NotNil(t, userToken)
	cookieUserToken := http.Cookie{Name: UserTokenCookieName, Value: userToken, Path: "/"}
	req.AddCookie(&cookieUserToken)

	userTokenCookieExists := false
	for _, cookie := range req.Cookies() {
		if cookie.Name == UserTokenCookieName {
			userTokenCookieExists = true
			break
		}
	}
	assert.True(t, userTokenCookieExists)

	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusOK, rr.Code)

	body, err := ioutil.ReadAll(rr.Body)
	assert.NoError(t, err)
	assert.Equal(t, "", string(body))

	userTokenCookieExpired := false
	for _, cookie := range rr.Result().Cookies() {
		if cookie.Name == UserTokenCookieName {
			if cookie.Expires.Before(time.Now()) {
				userTokenCookieExpired = true
			}
			break
		}
	}
	assert.True(t, userTokenCookieExpired)
}

func getTestConfig() (cfg.Config, error) {
	// Create temporary locations file
	fileLocations, err := ioutil.TempFile("", "*.xml")
	if err != nil {
		return nil, err
	}

	// Defer calls are stacked and will be executed in reverse order
	defer func() { _ = os.Remove(fileLocations.Name()) }()
	defer func() { _ = fileLocations.Close() }()

	_, err = fileLocations.WriteString(
		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
			"<locations>\n" +
			"    <location>\n" +
			"        <id>1</id>\n" +
			"        <name>DHBW Mosbach, A-Gebäude</name>\n" +
			"    </location>\n" +
			"</locations>",
	)
	if err != nil {
		return nil, err
	}

	// Create temporary file with TokenSignatureSecret
	fileSecret, err := ioutil.TempFile("", "*.txt")
	if err != nil {
		return nil, err
	}

	defer func() { _ = os.Remove(fileSecret.Name()) }()
	defer func() { _ = fileSecret.Close() }()

	secret := "secretFooBar"
	_, err = fileSecret.WriteString(secret)
	if err != nil {
		return nil, err
	}

	// Set the starting parameters
	args := make([]string, 6)
	args[0] = "-locationxmlpath"
	args[1] = fileLocations.Name()
	args[2] = "-qrvalidity"
	args[3] = "10000"
	args[4] = "-tokensecretpath"
	args[5] = fileSecret.Name()

	return cfg.NewConfig(args), nil
}
