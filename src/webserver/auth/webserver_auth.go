package auth

import (
	"fmt"
	"log"
	"net/http"
	cfg "prog2_go_programmentwurf/src/config"
	"prog2_go_programmentwurf/src/webserver"
)

func Start(cfg *cfg.Config) {
	http.HandleFunc((*cfg).LoginSitePath(), AuthHandler{cfg}.ServeLoginPage)
	http.HandleFunc(fmt.Sprintf("/api%v", (*cfg).LoginSitePath()), webserver.UserTokenWrapper(AuthHandler{cfg}.ServeLogin, cfg))
	http.HandleFunc((*cfg).LogoutSitePath(), AuthHandler{cfg}.ServeLogoutPage)
	http.HandleFunc(fmt.Sprintf("/api%v", (*cfg).LogoutSitePath()), AuthHandler{cfg}.ServeLogout)
	log.Fatalln(http.ListenAndServeTLS(fmt.Sprintf(":%v", (*cfg).LoginSitePort()), (*cfg).TlsCertificatePath(), (*cfg).TlsPrivateKeyPath(), nil))
}
