package webserver

import (
	"context"
	"net/http"
	"prog2_go_programmentwurf/src/auth"
	"prog2_go_programmentwurf/src/config"
	"prog2_go_programmentwurf/src/locations"
	"time"
)

type LocationIdKey string

const LocationIdContextKey LocationIdKey = "locationId"

// Current and previous tokens are valid
func isTokenExpired(payload locations.LoginTokenPayload, tokenValidityMs int64) bool {
	return payload.ExpiresAt+tokenValidityMs < time.Now().UnixMilli()
}

// TODO: write tests
func UserTokenWrapper(handler http.HandlerFunc, cfg *config.Config) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		token := r.URL.Query().Get("token")
		payload, err := auth.CheckLoginToken(token, (*cfg).TokenSignatureSecret())

		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			return
		}

		if isTokenExpired(payload, (*cfg).QrCodeValidityDurationMs()) {
			http.Error(w, "anmeldetoken ist abgelaufen", http.StatusUnauthorized)
			return
		}

		ctx := context.WithValue(r.Context(), LocationIdContextKey, payload.LocationId)
		handler(w, r.WithContext(ctx))
	}
}
