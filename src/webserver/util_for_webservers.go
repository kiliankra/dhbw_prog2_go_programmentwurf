/*
 * 2627585, 3110300, 2858031
 */

package webserver

import (
	"embed"
	"html/template"
	"log"
)

func LogResponseWriteError(_ int, err error) {
	if err != nil {
		log.Printf("Response write failed: %v", err)
	}
}

func LogParseTemplateError(err error) {
	if err != nil {
		log.Printf("Parsing template failed: %v", err)
	}
}

//go:embed templates
var templatesFs embed.FS

func GetTemplate(fileName string) *(template.Template) {
	return template.Must(template.ParseFS(templatesFs, "templates/"+fileName))
}
