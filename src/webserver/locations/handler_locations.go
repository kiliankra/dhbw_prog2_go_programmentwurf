/*
 * 2627585, 3110300, 2858031
 */

package locations

import (
	"net/http"
	"prog2_go_programmentwurf/src/config"
	"prog2_go_programmentwurf/src/locations"
	"prog2_go_programmentwurf/src/webserver"
	"strconv"
	"strings"
)

type LocationsHandler struct {
	cfg *config.Config
}

var templateLocations = webserver.GetTemplate("location.html")

// Handler to serve the HTML code for the locations page
func (locationsHandler LocationsHandler) ServeLocationsPage(w http.ResponseWriter, r *http.Request) {
	// Extracting the location id from the URL
	splittedUrl := strings.Split(r.URL.Path[1:], "/")
	locationIdAsString := splittedUrl[len(splittedUrl)-1]
	if locationIdAsString == "" {
		http.Error(w, "location id fehlt", http.StatusBadRequest)
		return
	}

	locationId, err := strconv.ParseInt(locationIdAsString, 10, 64)
	if err != nil {
		http.Error(w, "location id muss numerisch sein", http.StatusBadRequest)
		return
	}

	// Get the location content for the location page
	locationContent, err := locations.GetLocationContent(locationsHandler.cfg, locationId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	// Parse the template and write the result to the response writer
	webserver.LogParseTemplateError(templateLocations.ExecuteTemplate(w, "location.html", locationContent))
}
