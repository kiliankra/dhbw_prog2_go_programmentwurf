/*
 * 2627585, 3110300, 2858031
 */

package locations

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	cfg "prog2_go_programmentwurf/src/config"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Test for the handler_locations.go where location id is missing
func TestLocationsHandlerMissingLocationId(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.Nil(t, err)

	req, err := http.NewRequest("GET", fmt.Sprintf("%v/", testConfig.LocationSitePath()), nil)
	assert.NoError(t, err)
	assert.NotNil(t, req)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(LocationsHandler{&testConfig}.ServeLocationsPage)
	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusBadRequest, rr.Code)

	body, err := ioutil.ReadAll(rr.Body)
	assert.NoError(t, err)
	assert.Equal(t, "location id fehlt\n", string(body))
}

// Test for the handler_locations.go where location id is not a number
func TestLocationsHandlerLocationIdNotNumeric(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.Nil(t, err)

	req, err := http.NewRequest("GET", fmt.Sprintf("%v/a", testConfig.LocationSitePath()), nil)
	assert.NoError(t, err)
	assert.NotNil(t, req)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(LocationsHandler{&testConfig}.ServeLocationsPage)
	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusBadRequest, rr.Code)

	body, err := ioutil.ReadAll(rr.Body)
	assert.NoError(t, err)
	assert.Equal(t, "location id muss numerisch sein\n", string(body))
}

// Test for the handler_locations.go where location id invalid
func TestLocationsHandlerLocationIdNotFound(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.Nil(t, err)

	req, err := http.NewRequest("GET", fmt.Sprintf("%v/2", testConfig.LocationSitePath()), nil)
	assert.NoError(t, err)
	assert.NotNil(t, req)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(LocationsHandler{&testConfig}.ServeLocationsPage)
	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusNotFound, rr.Code)

	body, err := ioutil.ReadAll(rr.Body)
	assert.NoError(t, err)
	assert.Equal(t, "keine location mit der id 2 gefunden\n", string(body))
}

// Successful test for the handler_locations.go
func TestLocationsHandler(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.Nil(t, err)

	req, err := http.NewRequest("GET", fmt.Sprintf("%v/1", testConfig.LocationSitePath()), nil)
	assert.NoError(t, err)
	assert.NotNil(t, req)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(LocationsHandler{&testConfig}.ServeLocationsPage)
	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusOK, rr.Code)
}

func getTestConfig() (cfg.Config, error) {
	// Create temporary locations file
	fileLocations, err := ioutil.TempFile("", "*.xml")
	if err != nil {
		return nil, err
	}

	// Defer calls are stacked and will be executed in reverse order
	defer func() { _ = os.Remove(fileLocations.Name()) }()
	defer func() { _ = fileLocations.Close() }()

	_, err = fileLocations.WriteString(
		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
			"<locations>\n" +
			"    <location>\n" +
			"        <id>1</id>\n" +
			"        <name>DHBW Mosbach, A-Gebäude</name>\n" +
			"    </location>\n" +
			"</locations>",
	)
	if err != nil {
		return nil, err
	}

	// Create temporary file with TokenSignatureSecret
	fileSecret, err := ioutil.TempFile("", "*.txt")
	if err != nil {
		return nil, err
	}

	defer func() { _ = os.Remove(fileSecret.Name()) }()
	defer func() { _ = fileSecret.Close() }()

	secret := "secretFooBar"
	_, err = fileSecret.WriteString(secret)
	if err != nil {
		return nil, err
	}

	args := make([]string, 6)
	args[0] = "-locationxmlpath"
	args[1] = fileLocations.Name()
	args[2] = "-qrvalidity"
	args[3] = "10000"
	args[4] = "-tokensecretpath"
	args[5] = fileSecret.Name()

	return cfg.NewConfig(args), nil
}
