/*
 * 2627585, 3110300, 2858031
 */

package locations

import (
	"fmt"
	"log"
	"net/http"
	cfg "prog2_go_programmentwurf/src/config"
)

func Start(cfg *cfg.Config) {
	http.HandleFunc(fmt.Sprintf("%v/", (*cfg).LocationSitePath()), LocationsHandler{cfg}.ServeLocationsPage)
	log.Fatalln(http.ListenAndServeTLS(fmt.Sprintf(":%v", (*cfg).LocationSitePort()), (*cfg).TlsCertificatePath(), (*cfg).TlsPrivateKeyPath(), nil))
}
