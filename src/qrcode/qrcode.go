/*
 * 2627585, 3110300, 2858031
 */

package qrcode

import (
	"encoding/base64"
	"github.com/skip2/go-qrcode"
)

// Generates a new QR-Code with given content
func GenerateQrCode(content string) (string, error) {
	qrCodeBytes, err := qrcode.Encode(content, qrcode.Medium, 256)
	return base64.StdEncoding.EncodeToString(qrCodeBytes), err
}
