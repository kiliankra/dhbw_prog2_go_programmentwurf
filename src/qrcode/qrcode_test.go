/*
 * 2627585, 3110300, 2858031
 */

package qrcode

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGenerateQrCode(t *testing.T) {
	// Generates new QR-Code with the URL "https://github.com/hneemann" as content
	// and test if this runs without an Error
	generatedQrCode, err := GenerateQrCode("https://github.com/hneemann")
	assert.NoError(t, err)

	// Encodes the generated QR-Code with base64 and compares it with the already encoded expected QR-Code data
	const encodedTestQrCode = "iVBORw0KGgoAAAANSUhEUgAAAQAAAAEAAQMAAABmvDolAAAABlBMVEX///8AAABVwtN+AAABmklEQVR42uyZP5" +
		"KzMAzFnycFJUfwUXw0+2g+CkdwSZHx+0aSCcnHpt1Za1DBgPltsUJ/nhTcdtttf9ZWiu0rS3hiZc1yr0e+gA3AY18rOhZuia8jZ0Bif+yQK7" +
		"lB/DCOHAJr1afIcv6NQwDpCOTuFLAQTqVjaUDuP4f97MCoUTWH59JizeFLEZsbOLlANsnhr41nZkBCWF6lItdIK8U5cJ8KYB3/pj1Y3yzvQe" +
		"sBAMwPGH2zBLZYwSdcAeKHfgSt9M2u2KejPACkqqDcrW9qbub+oWkdANIksZAlnH0T4Ed2uwBKf7AlSkMRlbB7BezOXmVtmjKueAMI8YOFs0" +
		"h3LVvOACAV+dBDxqpTYs10BgyxoGLPaG2s7zXKAwDTd1aKVTnAJRA1WxuyTJ0bEPR5NmB81JpVHiQGzc3rADI3cM5ZInxiHTuQtxrlA3httM" +
		"YOJFxKsQ/gXHgsPKK6/CeTvACrDCVLE00r16sfnADU/SQrgsQ2rpu92YFjo2XJC5038yXsZweO3w44+qZOnfAG3Hbbbb9u/wIAAP//ACjPGs" +
		"+dRw0AAAAASUVORK5CYII="
	encodedGeneratedQrCode := generatedQrCode
	assert.Equal(t, encodedTestQrCode, encodedGeneratedQrCode)
}
