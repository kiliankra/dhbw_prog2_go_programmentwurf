/*
 * 2627585, 3110300, 2858031
 */

// The following JWT tokens were generated with: https://jwt.io

package token

import (
	"encoding/base64"
	"encoding/json"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

type TestJwt struct {
	header    string
	payload   string
	signature string
}

type TestPayload struct {
	CreatedAt int64 `json:"createdAt"`
	ExpiresAt int64 `json:"expiresAt"`
}

// Will get the base64 encoded parts (header,payload and signature) of the given JWT token.
// Fails the test if token format is invalid.
func getTestJwtContent(token string, t *testing.T) TestJwt {
	// check if token is valid jwt token (three parts seperated by a dot)
	splittedToken := strings.Split(token, ".")
	assert.Equal(t, 3, len(splittedToken))

	// check if token part is not empty
	for _, tokenPart := range splittedToken {
		assert.NotEqual(t, "", strings.TrimSpace(tokenPart))
	}

	return TestJwt{splittedToken[0], splittedToken[1], splittedToken[2]}
}

func TestTokenWhenPayloadIsNil(t *testing.T) {
	token, err := GenerateJwtToken(nil, "mySecret")
	assert.NotNil(t, err)
	assert.Equal(t, "", token)
}

func TestTokenWhenPayloadIsNotNil(t *testing.T) {
	payload := &TestPayload{0, 0}
	token, err := GenerateJwtToken(payload, "mySecret")

	assert.Nil(t, err)
	assert.NotEqual(t, "", token)
}

func TestTokenFormat(t *testing.T) {
	payload := &TestPayload{0, 0}
	token, _ := GenerateJwtToken(payload, "mySecret")

	// this method will test fail if format is invalid
	getTestJwtContent(token, t)
}

func TestTokenContent(t *testing.T) {
	payload := &TestPayload{123, 456}
	token, _ := GenerateJwtToken(payload, "mySecret")
	content := getTestJwtContent(token, t)
	encoder := base64.RawURLEncoding

	// check if token is base64 encoded
	tokenHeader, err := encoder.DecodeString(content.header)
	assert.Nil(t, err)

	tokenPayload, err := encoder.DecodeString(content.payload)
	assert.Nil(t, err)

	_, err = encoder.DecodeString(content.signature)
	assert.Nil(t, err)

	// check header
	parsedHeader := JwtTokenHeader{}
	err = json.Unmarshal(tokenHeader, &parsedHeader)
	assert.Nil(t, err)
	assert.Equal(t, "JWT", parsedHeader.Typ)
	assert.Equal(t, "HS256", parsedHeader.Alg)

	// check payload
	parsedPayload := TestPayload{}
	err = json.Unmarshal(tokenPayload, &parsedPayload)
	assert.Nil(t, err)
	assert.Equal(t, payload.CreatedAt, parsedPayload.CreatedAt)
	assert.Equal(t, payload.ExpiresAt, parsedPayload.ExpiresAt)
}

func TestVerifyTokenForMalformattedToken(t *testing.T) {
	type test struct {
		jwt    string
		secret string
	}

	tests := []test{
		{"", ""},
		{"a", ""},
		{"aa", ""},
		{"aaa", ""},
		{".a.a", ""},
		{"..a", ""},
		{"...", ""},
		{"a..a", ""},
		{"a..", ""},
		{".a.", ""},
	}

	for _, tc := range tests {
		err := VerifyJwtToken(tc.jwt, tc.secret, nil)
		assert.NotNil(t, err)
	}
}

func TestVerifyTokenWhenNotBase64(t *testing.T) {
	err := VerifyJwtToken("a.a.a", "", nil)
	assert.NotNil(t, err)
}

func TestVerifyTokenWhenHeaderDoesNotContainTypJwtAndAlgHs256(t *testing.T) {
	// token contains "typ: NOTJWT" and "alg: HS256", should give error because typ is not JWT
	err := VerifyJwtToken("eyJhbGciOiJIUzI1NiIsInR5cCI6Ik5PVEpXVCJ9.eyJ0ZXN0IjoiMTIzIn0.PGF46X_Rio0eX7G5paoD3uEm9Bxgg3Tvh6ZlVjp-oP8", "mySecret", nil)
	assert.NotNil(t, err)

	// token contains "typ: JWT" and "alg: HS512", should give error because alg is not HS256
	err = VerifyJwtToken("eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ0ZXN0IjoiMTIzIn0.YXVagGFOMm6uq9P6aUK8pHrEYaNujhnV7a8-HUwXun5wT4-bArmhjpZo08jupbEW3EsHu67J5H1MQ5qj_OJI6A", "mySecret", nil)
	assert.NotNil(t, err)
}

func TestVerifyTokenWhenPayloadChanged(t *testing.T) {
	err := VerifyJwtToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZXN0IjoiMTIzNDUifQ.rcsPCFkp1qGCcOU_hCBl_9skc6s57gFSJYl5rjlBJrY", "mySecret", nil)
	assert.NotNil(t, err)
}

func TestVerifyTokenWhenSignatureChanged(t *testing.T) {
	err := VerifyJwtToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZXN0IjoiMTIzIn0.rcsPCFkp1qGCcOU_hCBl_9skc6s57gFSJYl5rjlBJrY"+"invalidSignature", "mySecret", nil)
	assert.NotNil(t, err)
}

func TestVerifyToken(t *testing.T) {
	expectedPayload := TestPayload{123, 456}

	payload := TestPayload{}
	err := VerifyJwtToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjcmVhdGVkQXQiOjEyMywiZXhwaXJlc0F0Ijo0NTZ9.MeN3-Mea_QiJVC32rKcbrDJEjfnSJK6Q_j913o8UdKI", "mySecret", &payload)
	assert.Nil(t, err)
	assert.Equal(t, expectedPayload.CreatedAt, payload.CreatedAt)
	assert.Equal(t, expectedPayload.ExpiresAt, payload.ExpiresAt)
}

func TestSignature(t *testing.T) {
	signature := createSignature("mySecret", "MyData")

	// signature generated with: https://dinochiesa.github.io/hmachash/index.html
	expectedSignature := "0cAeQX7aLGFcI0Bh6mQV4Fn9_eBTeWlwN-j78wSDD6M"
	assert.Equal(t, expectedSignature, signature)
}
