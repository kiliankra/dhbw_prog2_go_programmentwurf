/*
 * 2627585, 3110300, 2858031
 */

package token

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"unicode/utf8"
)

// used https://www.digitalocean.com/community/tutorials/how-to-use-struct-tags-in-go to understand
// how to define fields name when encoding to json
type JwtTokenHeader struct {
	Alg string `json:"alg"`
	Typ string `json:"typ"`
}

// Generates a JWT with the given payload. Signature is created with the given secret
// and HS256 crypto algorithm. Used https://jwt.io for understanding how JWTs work
func GenerateJwtToken(payload interface{}, secret string) (string, error) {
	if payload == nil {
		return "", errors.New("jwt token kann nicht erstellt werden, da der payload leer ist")
	}

	jsonHeader, err := json.Marshal(JwtTokenHeader{"HS256", "JWT"})
	if err != nil {
		return "", err
	}

	jsonPayload, err := json.Marshal(payload)
	if err != nil {
		return "", err
	}

	encoder := base64.RawURLEncoding
	base64Header := encoder.EncodeToString(jsonHeader)
	base64Payload := encoder.EncodeToString(jsonPayload)
	signature := createSignature(secret, base64Header+"."+base64Payload)

	return fmt.Sprintf("%s.%s.%s", base64Header, base64Payload, signature), nil
}

// Checks if the given JWT token is valid for the given secret. Returns parsed JWT payload if valid, error otherwise.
func VerifyJwtToken(jwt string, secret string, v interface{}) error {
	splittedToken := strings.Split(jwt, ".")

	if len(splittedToken) != 3 {
		return errors.New("token format ungültig, er enthält keinen header, payload und Signatur. Token: " + jwt)
	}

	for _, tokenPart := range splittedToken {
		if utf8.RuneCountInString(strings.TrimSpace(tokenPart)) == 0 {
			return errors.New("token header, payload oder Signatur ist leer. Token: " + jwt)
		}
	}

	// check if token parts are base64 encoded
	header := splittedToken[0]
	payload := splittedToken[1]
	signature := splittedToken[2]
	encoder := base64.RawURLEncoding

	// verify that token parts are base64 encoded
	headerBytes, err := encoder.DecodeString(header)
	if err != nil {
		return errors.New("token header ist nicht base64 kodiert, header:  " + header)
	}

	payloadBytes, err := encoder.DecodeString(payload)
	if err != nil {
		return errors.New("token payload ist nicht base64 kodiert, payload: " + payload)
	}

	_, err = encoder.DecodeString(signature)
	if err != nil {
		return errors.New("token Signatur ist nicht base64 kodiert, signature: " + signature)
	}

	// verify that header and payload are valid json objects
	parsedHeader := JwtTokenHeader{}
	err = json.Unmarshal(headerBytes, &parsedHeader)
	if err != nil {
		return errors.New("token header konnte nicht gelesen werden. Header: " + string(headerBytes))
	}

	err = json.Unmarshal(payloadBytes, v)
	if err != nil {
		return errors.New("token payload konnte nicht gelesen werden. Payload: " + string(payloadBytes))
	}

	// verify that header contains typ: JWT and alg: HS256
	if parsedHeader.Typ != "JWT" {
		return errors.New("token header typ ist nicht 'JWT'. Header: " + string(headerBytes))
	}

	if parsedHeader.Alg != "HS256" {
		return errors.New("jwt header enthhält einen nicht unterstützten Algorithmus. Nur HS256 ist unterstützt. Header: " + string(headerBytes))
	}

	// check if signature is valid / if payload was changed
	expectedSignature := createSignature(secret, header+"."+payload)

	if string(signature) != expectedSignature {
		return errors.New("token Signatur ist ungültig. Token: " + jwt)
	}

	return nil
}

// Creates a signature for the given data with the given secret. See https://pkg.go.dev/crypto/hmac.
func createSignature(secret string, data string) string {
	h := hmac.New(sha256.New, []byte(secret))
	h.Write([]byte(data))

	return base64.RawURLEncoding.EncodeToString(h.Sum(nil))
}
