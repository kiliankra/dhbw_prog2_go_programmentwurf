/*
 * 2627585, 3110300, 2858031
 */

package config

import (
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"testing"
)

// Testinhalt für eine gültige locations.xml
const testXmlString = "" +
	"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
	"<locations>\n" +
	"    <location>\n" +
	"        <id>1</id>\n" +
	"        <name>DHBW Mosbach, A-Gebäude</name>\n" +
	"    </location>\n" +
	"    <location>\n" +
	"        <id>3</id>\n" +
	"        <name>DHBW Mosbach, E-Gebäude</name>\n" +
	"    </location>\n" +
	"</locations>"

// In diesem Test wird geprüft, dass die Locations aus einer XML-Datei korrekt gelesen werden
func TestLoadLocations(t *testing.T) {
	// Vorbereitung: Schreibe eine (temporäre) XML-Datei
	file, err := ioutil.TempFile("", "*.xml")
	assert.NotNil(t, file)
	assert.Nil(t, err)
	// Cleanup mit mehreren defer-Aufrufen → Ausführung in umgekehrter Reihenfolge
	defer func() { _ = os.Remove(file.Name()) }()
	defer func() { _ = file.Close() }()

	_, err = file.WriteString(testXmlString)
	assert.Nil(t, err)

	// Lade die Orte von der temporären Datei und prüfe, dass diese richtig gelesen wurden
	locations := LoadLocations(file.Name())

	assert.Equal(t, 2, len(locations.Locations))
	assert.Equal(t, int64(1), locations.Locations[0].Id)
	assert.Equal(t, "DHBW Mosbach, A-Gebäude", locations.Locations[0].Name)
	assert.Equal(t, int64(3), locations.Locations[1].Id)
	assert.Equal(t, "DHBW Mosbach, E-Gebäude", locations.Locations[1].Name)
}

// Fehlerfall: Angegebene Datei existiert nicht
func TestLoadLocationsFileDoesNotExist(t *testing.T) {
	locations := LoadLocations("thisFileDoesNotExist.xml")
	assert.Equal(t, 0, len(locations.Locations))
}

// Fehlerfall: Angegebene Datei enthält keine gültigen Daten
func TestLoadLocationsFileNotValid(t *testing.T) {
	// Vorbereitung: Schreibe eine (temporäre) XML-Datei
	file, err := ioutil.TempFile("", "*.xml")
	assert.NotNil(t, file)
	assert.Nil(t, err)
	// Cleanup mit mehreren defer-Aufrufen → Ausführung in umgekehrter Reihenfolge
	defer func() { _ = os.Remove(file.Name()) }()
	defer func() { _ = file.Close() }()

	_, err = file.WriteString("thisIsNotXmlThereforeThisIsInvalid")
	assert.Nil(t, err)

	// Lade die Orte von der temporären Datei und prüfe, dass diese richtig gelesen wurden
	locations := LoadLocations(file.Name())
	assert.Equal(t, 0, len(locations.Locations))
}
