/*
 * 2627585, 3110300, 2858031
 */

package config

import (
	"io/ioutil"
	"os"
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Prüft, dass eine Instanz von impl einer Variable vom Typ Config zugewiesen werden kann.
// Dieser Test schlägt bereits zur Compile-Time fehl, falls eine Methode nicht implementiert ist.
func TestConfigIsInterface(t *testing.T) {
	args := make([]string, 0)
	var config = NewConfig(args)
	if config == nil {
		t.Fail()
	}

	// Prüfe, dass die Print-Methode keine Fehler liefert
	PrintConfig(config)
}

// Prüft, dass alle Werte in der Konfiguration bei einem Aufruf ohne weitere Parameter auf die korrekten Standardwerte gesetzt werden.
func TestDefaultConfig(t *testing.T) {
	args := make([]string, 0)
	config := NewConfig(args)
	assert.Equal(t, "localhost", config.ServerHostAddress())
	assert.Equal(t, int16(8080), config.LocationSitePort())
	assert.Equal(t, int16(8081), config.LoginSitePort())

	assert.Equal(t, "/locations", config.LocationSitePath())
	assert.Equal(t, "/login", config.LoginSitePath())
	assert.Equal(t, "/logout", config.LogoutSitePath())

	assert.Equal(t, ".data/locations.xml", config.LocationXmlFilePath())
	assert.Empty(t, config.Locations()) // Keine XML an der Standardposition für die Testumgebung

	assert.Equal(t, int64(60000), config.QrCodeValidityDurationMs())

	assert.Equal(t, "journal", config.JournalDirectoryPath())

	assert.Equal(t, ".data/tls/cert.pem", config.TlsCertificatePath())
	assert.Equal(t, ".data/tls/key.pem", config.TlsPrivateKeyPath())
	assert.Equal(t, "secret.txt", config.TokenSignatureSecretPath())
	matches, err := regexp.Match(".+", []byte(config.TokenSignatureSecret()))
	assert.Nil(t, err)
	assert.True(t, matches)
}

// Prüft, dass die Ausführung fehlschlägt, wenn ein fehlerhafter Parameter übergeben wird.
func TestParsingFailed(t *testing.T) {
	args := make([]string, 2)
	args[0] = "-thisflagdoesnotexist"
	args[1] = "thereisnovalue"
	assert.Panics(t, func() {
		NewConfig(args)
	})
}

// Die folgenden Methoden prüfen, dass die jeweiligen Einzelparameter korrekt eingelesen werden.
// Zudem wird die Fehlerbehandlung von Spezialfällen geprüft.

func TestServerHostAddress(t *testing.T) {
	args := make([]string, 2)
	args[0] = "-hostname"

	// Standardfall: DNS-Name
	args[1] = "www.checkin.mosbach.dhbw.de"
	config := NewConfig(args)
	assert.Equal(t, "www.checkin.mosbach.dhbw.de", config.ServerHostAddress())

	// Standardfall: IP-Adresse
	args[1] = "192.168.178.24"
	config = NewConfig(args)
	assert.Equal(t, "192.168.178.24", config.ServerHostAddress())
}

func TestQrSitePort(t *testing.T) {
	args := make([]string, 2)
	args[0] = "-locationport"

	// Standardfall: Gültiger Wert für den Port
	args[1] = "7271"
	config := NewConfig(args)
	assert.Equal(t, int16(7271), config.LocationSitePort())

	// Ungültiger Fall: Port größer als der Maximalwert
	args[1] = "222222"
	config = NewConfig(args)
	// Erwartet: Standardwert
	assert.Equal(t, int16(8080), config.LocationSitePort())

	// Ungültiger Fall: Port kleiner als der Minimalwert
	args[1] = "-124"
	config = NewConfig(args)
	// Erwartet: Standardwert
	assert.Equal(t, int16(8080), config.LocationSitePort())
}

func TestLoginSitePort(t *testing.T) {
	args := make([]string, 2)
	args[0] = "-loginport"

	// Standardfall: Gültiger Wert für den Port
	args[1] = "6109"
	config := NewConfig(args)
	assert.Equal(t, int16(6109), config.LoginSitePort())

	// Ungültiger Fall: Port größer als der Maximalwert
	args[1] = "222222"
	config = NewConfig(args)
	// Erwartet: Standardwert
	assert.Equal(t, int16(8081), config.LoginSitePort())

	// Ungültiger Fall: Port kleiner als der Minimalwert
	args[1] = "-124"
	config = NewConfig(args)
	// Erwartet: Standardwert
	assert.Equal(t, int16(8081), config.LoginSitePort())
}

func TestLocationSitePath(t *testing.T) {
	args := make([]string, 2)
	args[0] = "-locationpath"

	// Standardfall: Gültiger URL-Pfad
	args[1] = "/qrcode"
	config := NewConfig(args)
	assert.Equal(t, "/qrcode", config.LocationSitePath())

	// Sonderfall: Forward-Slash ist am Anfang nicht angegeben oder am Ende des Pfads angegeben
	args[1] = "qrdisplay/"
	config = NewConfig(args)
	assert.Equal(t, "/qrdisplay", config.LocationSitePath())

	args[1] = "/display/"
	config = NewConfig(args)
	assert.Equal(t, "/display", config.LocationSitePath())

	args[1] = "code"
	config = NewConfig(args)
	assert.Equal(t, "/code", config.LocationSitePath())

	// Sonderfall: Angegebene URL enthält ungültige Zeichen (z.B. Leerzeichen)
	args[1] = "QR code"
	config = NewConfig(args)
	assert.Equal(t, "/QR%20code", config.LocationSitePath())
}

func TestLoginSitePath(t *testing.T) {
	args := make([]string, 2)
	args[0] = "-loginpath"

	// Standardfall: Gültiger URL-Pfad
	args[1] = "/checkin"
	config := NewConfig(args)
	assert.Equal(t, "/checkin", config.LoginSitePath())

	// Sonderfall: Forward-Slash ist am Anfang nicht angegeben oder am Ende des Pfads angegeben
	args[1] = "arrive/"
	config = NewConfig(args)
	assert.Equal(t, "/arrive", config.LoginSitePath())

	args[1] = "/entrance/"
	config = NewConfig(args)
	assert.Equal(t, "/entrance", config.LoginSitePath())

	args[1] = "enter"
	config = NewConfig(args)
	assert.Equal(t, "/enter", config.LoginSitePath())

	// Sonderfall: Angegebene URL enthält ungültige Zeichen (z.B. Leerzeichen)
	args[1] = "Login Page/checkin"
	config = NewConfig(args)
	assert.Equal(t, "/Login%20Page/checkin", config.LoginSitePath())
}

func TestLogoutSitePath(t *testing.T) {
	args := make([]string, 2)
	args[0] = "-logoutpath"

	// Standardfall: Gültiger URL-Pfad
	args[1] = "/checkout"
	config := NewConfig(args)
	assert.Equal(t, "/checkout", config.LogoutSitePath())

	// Sonderfall: Forward-Slash ist am Anfang nicht angegeben oder am Ende des Pfads angegeben
	args[1] = "depart/"
	config = NewConfig(args)
	assert.Equal(t, "/depart", config.LogoutSitePath())

	args[1] = "/exit/"
	config = NewConfig(args)
	assert.Equal(t, "/exit", config.LogoutSitePath())

	args[1] = "leave"
	config = NewConfig(args)
	assert.Equal(t, "/leave", config.LogoutSitePath())

	// Sonderfall: Angegebene URL enthält ungültige Zeichen (z.B. Leerzeichen)
	args[1] = "Logout Page/checkout"
	config = NewConfig(args)
	assert.Equal(t, "/Logout%20Page/checkout", config.LogoutSitePath())
}

func TestLocationXmlFilePath(t *testing.T) {
	doFilePathParameterTests(t, "-locationxmlpath", func(c Config) string {
		return c.LocationXmlFilePath()
	})
}

func TestLocations(t *testing.T) {
	file, err := ioutil.TempFile("", "*.xml")
	assert.NotNil(t, file)
	assert.Nil(t, err)
	// Stacked defer calls -> executed in reverse order
	defer func() { _ = os.Remove(file.Name()) }()
	defer func() { _ = file.Close() }()

	_, err = file.WriteString(
		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
			"<locations>\n" +
			"    <location>\n" +
			"        <id>2</id>\n" +
			"        <name>DHBW Mosbach, B-Gebäude</name>\n" +
			"    </location>\n" +
			"    <location>\n" +
			"        <id>6</id>\n" +
			"        <name>DHBW Mosbach, Bibliothek</name>\n" +
			"    </location>\n" +
			"</locations>",
	)
	assert.Nil(t, err)

	args := make([]string, 2)
	args[0] = "-locationxmlpath"
	args[1] = file.Name()
	config := NewConfig(args)
	assert.Equal(t, 2, len(config.Locations()))
	assert.Equal(t, int64(2), config.Locations()[0].Id)
	assert.Equal(t, "DHBW Mosbach, B-Gebäude", config.Locations()[0].Name)
	assert.Equal(t, int64(6), config.Locations()[1].Id)
	assert.Equal(t, "DHBW Mosbach, Bibliothek", config.Locations()[1].Name)

	location, err := config.GetLocationById(2)
	assert.Nil(t, err)
	assert.Equal(t, int64(2), location.Id)
	assert.Equal(t, "DHBW Mosbach, B-Gebäude", location.Name)

	location, err = config.GetLocationById(-1)
	assert.NotNil(t, err)
	assert.Equal(t, int64(0), location.Id)
	assert.Equal(t, "", location.Name)
}

func TestQrCodeValidityDurationMs(t *testing.T) {
	args := make([]string, 2)
	args[0] = "-qrvalidity"

	// Standardfall: Gültiger Wert für den Port
	args[1] = "10000"
	config := NewConfig(args)
	assert.Equal(t, int64(10000), config.QrCodeValidityDurationMs())
}

func TestJournalDirectoryPath(t *testing.T) {
	doFilePathParameterTests(t, "-journalpath", func(c Config) string {
		return c.JournalDirectoryPath()
	})
}

func TestTlsCertificatePath(t *testing.T) {
	doFilePathParameterTests(t, "-tlscert", func(c Config) string {
		return c.TlsCertificatePath()
	})
}

func TestTlsPrivateKeyPath(t *testing.T) {
	doFilePathParameterTests(t, "-tlskey", func(c Config) string {
		return c.TlsPrivateKeyPath()
	})
}

func TestTokenSignatureSecretPath(t *testing.T) {
	doFilePathParameterTests(t, "-tokensecretpath", func(c Config) string {
		return c.TokenSignatureSecretPath()
	})
}

func TestTokenSignatureSecret(t *testing.T) {
	file, err := ioutil.TempFile("", "*.txt")
	assert.NotNil(t, file)
	assert.Nil(t, err)
	// Stacked defer calls -> executed in reverse order
	defer func() { _ = os.Remove(file.Name()) }()
	defer func() { _ = file.Close() }()

	secret := "thisisaveryspecialsecret123SADFIASON"
	_, err = file.WriteString(secret)
	assert.Nil(t, err)

	args := make([]string, 2)
	args[0] = "-tokensecretpath"
	args[1] = file.Name()
	config := NewConfig(args)
	assert.Equal(t, secret, config.TokenSignatureSecret())

	// Prüfe, dass eine neue Datei erzeugt wird, wenn der Pfad noch nicht existiert.
	dir, err := ioutil.TempDir("", "*")
	assert.Nil(t, err)
	defer func() { _ = os.RemoveAll(dir) }()

	args[1] = dir + "/secret.txt"
	config = NewConfig(args)

	data, err := ioutil.ReadFile(dir + "/secret.txt")
	assert.Nil(t, err)
	assert.NotEmpty(t, config.TokenSignatureSecret())
	assert.Equal(t, string(data), config.TokenSignatureSecret())

	// Prüfe, dass bei einer fehlerhaften Angabe (existierendes Verzeichnis) ein zufälliges Secret generiert wird
	args[1] = os.TempDir()
	config = NewConfig(args)
	assert.NotEmpty(t, config.TokenSignatureSecret())

	// Prüfe, dass bei einer fehlerhaften Angabe (Pfad enthält eine existierende Datei) ein zufälliges Secret generiert wird
	file, err = ioutil.TempFile("", "*")
	assert.Nil(t, err)
	assert.NotNil(t, file)
	// Stacked defer calls -> executed in reverse order
	defer func() { _ = os.Remove(file.Name()) }()
	defer func() { _ = file.Close() }()

	args[1] = file.Name() + "/secret.txt"
	config = NewConfig(args)
	assert.NotEmpty(t, config.TokenSignatureSecret())
}

func doFilePathParameterTests(t *testing.T, argname string, accessor func(c Config) string) {
	args := make([]string, 2)
	args[0] = argname

	// Standardfall: Gültiger, relativer Pfad
	args[1] = ".data/datafile.xml"
	config := NewConfig(args)
	assert.Equal(t, ".data/datafile.xml", accessor(config))

	// Spezialfälle: Absolute Pfade unter Windows/Linux
	args[1] = "C:\\Users\\Default\\Documents\\file.txt"
	config = NewConfig(args)
	assert.Equal(t, "C:\\Users\\Default\\Documents\\file.txt", accessor(config))

	args[1] = "/home/root/file"
	config = NewConfig(args)
	assert.Equal(t, "/home/root/file", accessor(config))
}
