/*
 * 2627585, 3110300, 2858031
 */

package config

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/url"
	"os"
	pathpkg "path"
	"reflect"
	"strings"
	"time"
)

const DefaultLocationPort = 8080
const DefaultLoginPort = 8081

const GeneratedSecretLength = 512

// Config Schnittstelle für die Abfrage von eingestellten Werten.
// 	      Die Nutzereingabe wird bereits so verarbeitet, dass diese Werte direkt nutzbar sind.
//        Falls durch den Nutzer keine Eingabe erfolgt, werden Standardwerte verwendet.
//        Nach der Initialisierung werden die abgespeicherten Werte nicht mehr verändert.
type Config interface {
	ServerHostAddress() string

	LocationSitePort() int16
	LoginSitePort() int16

	LocationSitePath() string
	LoginSitePath() string
	LogoutSitePath() string

	LocationXmlFilePath() string
	Locations() []Location

	QrCodeValidityDurationMs() int64

	JournalDirectoryPath() string

	TlsCertificatePath() string
	TlsPrivateKeyPath() string

	TokenSignatureSecretPath() string
	TokenSignatureSecret() string

	GetLocationById(id int64) (Location, error)
}

// impl Struktur/Klasse zur Realisierung der Config-Schnittstelle.
type impl struct {
	serverHostAddress string

	locationSitePort int16
	loginSitePort    int16

	locationSitePath string
	loginSitePath    string
	logoutSitePath   string

	locationXmlFilePath string
	locations           []Location

	qrCodeValidityDurationMs int64

	journalDirectoryPath string

	tlsCertificatePath string
	tlsPrivateKeyPath  string

	tokenSignatureSecretPath string
	tokenSignatureSecret     string
}

// NewConfig liest die Einstellungen aus den angegebenen Argumenten und erstellt eine neue Config-Instanz.
//           Dabei löst die Methode keine Panic aus, wenn Fehlkonfigurationen erkannt werden, sondern versucht den Fehler so sinnvoll wie möglich zu korrigieren.
//           Diese Korrekturen werden dem Nutzer mitgeteilt, damit dieser die Fehlkonfigurationen bei Bedarf korrigieren kann.
func NewConfig(args []string) Config {
	// Parser vorbereiten
	flags := flag.NewFlagSet("configFlags", flag.ContinueOnError)

	// Parameter/Argumente definieren
	hostName := flags.String("hostname", "localhost", "Hostname unter dem der Server erreicht werden kann.")

	locationSitePort := flags.Int("locationport", DefaultLocationPort, "Port unter dem die QR-Code-Anzeige erreicht werden kann (gültige Werte: 0-65535)")
	loginSitePort := flags.Int("loginport", DefaultLoginPort, "Port unter dem die Login/Logout-Funktion erreicht werden kann (gültige Werte: 0-65535)")

	locationSitePath := flags.String("locationpath", "/locations", "Pfad unter dem die Anzeige des QR-Codes zu finden ist (wird mit der ID des Ortes ergänzt)")
	loginSitePath := flags.String("loginpath", "/login", "Pfad unter dem die Anzeige des QR-Codes zu finden ist")
	logoutSitePath := flags.String("logoutpath", "/logout", "Pfad unter dem die Anzeige des QR-Codes zu finden ist")

	locationXmlPath := flags.String("locationxmlpath", ".data/locations.xml", "Pfad zur XML-Datei mit den unterstützten Orten")

	qrValidity := flags.Int64("qrvalidity", 60000, "Maximale Anzeigedauer des QR-Codes in Millisekunden (die Gültigkeit ist das doppelte der Anzeigedauer)")

	journalPath := flags.String("journalpath", "journal", "Verzeichnis in das die Journallogs abgelegt werden")
	tlsCertPath := flags.String("tlscert", ".data/tls/cert.pem", "Zertifikat für die HTTPS- bzw. TLS/SSL-Verschlüsselung")
	tlsKeyPath := flags.String("tlskey", ".data/tls/key.pem", "Privater Schlüssel für die HTTPS- bzw. TLS/SSL-Verschlüsselung")
	tokenSecretPath := flags.String("tokensecretpath", "secret.txt", "Geheimer Schlüssel für die Erstellung/Signierung von Token (wird generiert, falls die Datei nicht existiert)")

	// Lese Parameter und speichere sie
	err := flags.Parse(args)
	if err != nil {
		log.Panicf("Fehler bei der Verarbeitung der Parameter: \"%v\"", err)
	}

	// Erstelle Struktur und übertrage/lade Daten
	c := &impl{}
	c.serverHostAddress = *hostName
	c.locationSitePort = getPort(*locationSitePort, DefaultLocationPort)
	c.loginSitePort = getPort(*loginSitePort, DefaultLoginPort)
	c.locationSitePath = getUrlPath(*locationSitePath)
	c.loginSitePath = getUrlPath(*loginSitePath)
	c.logoutSitePath = getUrlPath(*logoutSitePath)
	c.locationXmlFilePath = getFilePath(*locationXmlPath)
	c.qrCodeValidityDurationMs = getMsDuration(*qrValidity)
	c.journalDirectoryPath = getFilePath(*journalPath)
	c.tlsCertificatePath = getFilePath(*tlsCertPath)
	c.tlsPrivateKeyPath = getFilePath(*tlsKeyPath)
	c.tokenSignatureSecretPath = getFilePath(*tokenSecretPath)
	c.tokenSignatureSecret = getSecret(c.tokenSignatureSecretPath)
	c.locations = LoadLocations(c.LocationXmlFilePath()).Locations

	return c
}

// getPort prüft die Nutzereingabe für Ports und behandelt mögliche Fehlerfälle
func getPort(port int, defaultValue int16) int16 {
	if port < 0 || port > 65535 {
		log.Printf("WARN: Angegebener Port %v außerhalb des Wertebereichs. Der Standardwert %v wird benutzt.\n",
			port, defaultValue)
		return defaultValue
	} else {
		return int16(port)
	}
}

// getUrlPath prüft die Nutzereingabe für URL-Pfade, escaped ungültige Zeichen und prüft, dass das Format den Erwartungen entspricht
func getUrlPath(path string) string {
	// First escape possible bad characters
	modifiedPath := url.PathEscape(path)
	// Undo false escaping of path separators
	modifiedPath = strings.ReplaceAll(modifiedPath, "%2F", "/")
	modifiedPath = pathpkg.Clean(modifiedPath)

	// Check if start is a forward-slash
	if !strings.HasPrefix(path, "/") {
		modifiedPath = "/" + modifiedPath
	}

	return modifiedPath
}

// getFilePath prüft die Nutzereingabe für Dateipfade und behandelt mögliche Fehlerfälle.
//             Aktuell ist dies eine NOOP, da für Dateipfade keine besonderen Voraussetzungen gelten.
//             Die Methode wurde dennoch bereits angelegt, um mögliche (zukünftige) Erweiterungen zu vereinfachen.
func getFilePath(path string) string {
	return path
}

// getMsDuration prüft die Nutzereingabe für Zeitdauern und behandelt mögliche Fehlerfälle.
//               Aktuell ist dies eine NOOP, da für Dateipfade keine besonderen Voraussetzungen gelten.
//               Die Methode wurde dennoch bereits angelegt, um mögliche (zukünftige) Erweiterungen zu vereinfachen.
func getMsDuration(duration int64) int64 {
	return duration
}

// getSecret lädt den Geheimschlüssel aus der angegebenen Datei (oder erstellt diese Datei und generiert einen Schlüssel)
func getSecret(path string) string {
	info, err := os.Stat(path)
	if os.IsNotExist(err) || info == nil {
		parentPath := pathpkg.Dir(path)

		err := os.MkdirAll(parentPath, os.ModeDir)
		if err != nil {
			log.Printf("ERROR: Fehler beim Anlegen der Secret-Datei: \"%v\"\n", err)
			return generateSecret(GeneratedSecretLength)
		}

		file, err := os.Create(path)
		if err != nil {
			log.Printf("ERROR: Fehler beim Anlegen der Secret-Datei: \"%v\"\n", err)
			return generateSecret(GeneratedSecretLength)
		}
		defer func() { _ = file.Close() }()

		generated := generateSecret(GeneratedSecretLength)
		_, err = file.WriteString(generated)
		if err != nil {
			log.Printf("ERROR: Fehler beim Schreiben in die Secret-Datei: \"%v\"\n", err)
		}
		return generated
	} else if info.IsDir() {
		log.Printf("ERROR: Die angegebene Secret-Datei existiert und ist ein Verzeichnis")
		return generateSecret(GeneratedSecretLength)
	} else {
		// File exists and is a file
		file, err := os.Open(path)
		if err != nil {
			log.Printf("ERROR: Fehler beim Lesen der Secret-Datei: \"%v\"\n", err)
			return generateSecret(GeneratedSecretLength)
		}
		defer func() { _ = file.Close() }()

		secret, err := ioutil.ReadAll(file)
		if err != nil {
			log.Printf("ERROR: Fehler beim Lesen der Secret-Datei: \"%v\"\n", err)
			return generateSecret(GeneratedSecretLength)
		}
		return string(secret)
	}
}

func generateSecret(length int) string {
	characters := []byte("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	count := len(characters)
	rand.Seed(time.Now().UnixNano())

	result := make([]byte, length)

	for i := 0; i < length; i++ {
		result[i] = characters[rand.Intn(count)]
	}

	return string(result)
}

// PrintConfig druckt die aktuellen Werte der Config.
//             Hierzu wird Reflection verwendet, somit ist es nicht nötig diese Methode anzupassen, wenn sich die Werte der Config ändern.
func PrintConfig(c Config) {
	toOutput := "Einstellungen in der aktuellen Konfiguration:"
	configType := reflect.TypeOf(c)

	for i := 0; i < configType.NumMethod(); i++ {
		method := configType.Method(i)

		// Es sollen nur Getter aufgerufen werden.
		// Diese sind daran zu erkennen, dass sie nur 1 Argument haben (das Objekt selbst)
		input := []reflect.Value{reflect.ValueOf(c)}
		if method.Type.NumIn() == 1 {
			val := method.Func.Call(input)[0]

			// Darstellung mit Anführungszeichen, falls es sich um einen String handelt
			if val.Kind() == reflect.String {
				toOutput += fmt.Sprintf("\n  %v: \"%v\"", method.Name, val)
			} else {
				toOutput += fmt.Sprintf("\n  %v: %v", method.Name, val)
			}
		}
	}

	log.Println(toOutput)
}

// Gettermethoden (generiert durch die IDE)

func (c *impl) ServerHostAddress() string {
	return c.serverHostAddress
}

func (c *impl) LocationSitePort() int16 {
	return c.locationSitePort
}

func (c *impl) LoginSitePort() int16 {
	return c.loginSitePort
}

func (c *impl) LocationSitePath() string {
	return c.locationSitePath
}

func (c *impl) LoginSitePath() string {
	return c.loginSitePath
}

func (c *impl) LogoutSitePath() string {
	return c.logoutSitePath
}

func (c *impl) LocationXmlFilePath() string {
	return c.locationXmlFilePath
}

func (c *impl) Locations() []Location {
	return c.locations
}

func (c *impl) QrCodeValidityDurationMs() int64 {
	return c.qrCodeValidityDurationMs
}

func (c *impl) JournalDirectoryPath() string {
	return c.journalDirectoryPath
}

func (c *impl) TlsCertificatePath() string {
	return c.tlsCertificatePath
}

func (c *impl) TlsPrivateKeyPath() string {
	return c.tlsPrivateKeyPath
}

func (c *impl) TokenSignatureSecretPath() string {
	return c.tokenSignatureSecretPath
}

func (c *impl) TokenSignatureSecret() string {
	return c.tokenSignatureSecret
}

func (c *impl) GetLocationById(id int64) (Location, error) {
	for i := range c.locations {
		if c.locations[i].Id == id {
			return c.locations[i], nil
		}
	}

	return Location{}, fmt.Errorf("keine location mit der id %v gefunden", id)
}
