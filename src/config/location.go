/*
 * 2627585, 3110300, 2858031
 */

package config

import (
	"encoding/xml"
	"io/ioutil"
	"log"
	"os"
)

// Annotierte Strukturen für die Nutzung des XML-Parsers aus der Standardbibliothek

type Locations struct {
	XMLName   xml.Name   `xml:"locations"`
	Locations []Location `xml:"location"`
}

type Location struct {
	XMLName xml.Name `xml:"location"`
	Id      int64    `xml:"id"`
	Name    string   `xml:"name"`
}

func LoadLocations(locationXmlPath string) Locations {
	errorReturnValue := Locations{Locations: []Location{}}
	xmlFile, err := os.Open(locationXmlPath)

	// Fehlerfall: Datei kann nicht gelesen/geöffnet werden -> Fehlermeldung + leere Rückgabe
	if err != nil {
		log.Printf("WARN: Fehler beim Öffnen der Locations-Datei: \"%v\". Es konnten keine Orte geladen werden.\n", err)
		return errorReturnValue
	}

	// Schließe Datei am Ende der Funktion
	defer func() { _ = xmlFile.Close() }()

	byteValue, err := ioutil.ReadAll(xmlFile)
	if err != nil {
		log.Printf("WARN: Fehler beim Lesen der Locations-Datei: \"%v\". Es konnten keine Orte geladen werden.\n", err)
		return errorReturnValue
	}

	var locations Locations
	err = xml.Unmarshal(byteValue, &locations)
	if err != nil {
		log.Printf("WARN: Fehler beim Parsen der Locations-Datei: \"%v\". Es konnten keine Orte geladen werden.\n", err)
		return errorReturnValue
	}

	return locations
}
