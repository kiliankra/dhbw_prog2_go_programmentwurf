/*
 * 2627585, 3110300, 2858031
 */

package analysis

import (
	"github.com/stretchr/testify/assert"
	"prog2_go_programmentwurf/src/journal"
	"sort"
	"testing"
	"time"
)

// In diesem Test wird überprüft, dass eine Instanz des Analyzers korrekt angelegt wird und die Initialisierung verläuft wie erwartet.
func TestNewAnalyzer(t *testing.T) {
	var analyzer Analyzer
	analyzer = NewAnalyzer(getTestSet())
	assert.NotNil(t, analyzer)

	// Prüfe, dass die Eingabedaten zunächst sortiert werden
	testSet := getTestSet()
	sort.Sort(testSet)
	assert.Equal(t, testSet, analyzer.getRecords())
}

// In diesem Test wird überprüft, dass alle Personen korrekt aus den Datensätzen extrahiert werden
func TestAnalyzerGetPeople(t *testing.T) {
	analyzer := NewAnalyzer(getTestSet())

	expectedPeople := []Person{
		{"Dirk Saller", "Lohrtalweg 10 Mosbach"},
		{"Alexander Auch", "Lohrtalweg 10 Mosbach"},
		{"Dirk Saller", "Lohrtalweg 14 Neckarelz"},
		{"Marie Schreiber", "Koenigstrasse 72 96359 Steinbach"},
		{"Martina Fuchs", "Hermannstrasse 5 68649 Groß-Rohrheim"},
	}

	assert.ElementsMatch(t, expectedPeople, analyzer.GetPeople())
}

// In diesem Test wird überprüft, dass alle Orte korrekt aus den Datensätzen extrahiert werden
func TestAnalyzerGetLocations(t *testing.T) {
	analyzer := NewAnalyzer(getTestSet())

	expectedLocations := []string{
		"DHBW Mosbach A-Gebäude",
		"DHBW Mosbach B-Gebäude",
		"DHBW Mosbach E-Gebäude",
		"DHBW Mosbach Mensa",
		"Alte Mälzerei Mosbach",
		"\"Tante Gerda\" Mosbach",
	}

	assert.ElementsMatch(t, expectedLocations, analyzer.GetLocations())
}

// In diesem Test wird überprüft, dass alle Datensätze für eine Person korrekt gefunden werden
func TestAnalyzerGetRecordsForPerson(t *testing.T) {
	analyzer := NewAnalyzer(getTestSet())
	testPerson := Person{"Alexander Auch", "Lohrtalweg 10 Mosbach"}

	expected := JournalEntries{
		{parseTime("2021-11-18-08:10:20"), journal.EntryModeCheckIn, "DHBW Mosbach E-Gebäude", "Alexander Auch", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-18-08:13:20"), journal.EntryModeCheckOut, "DHBW Mosbach E-Gebäude", "Alexander Auch", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-18-08:14:20"), journal.EntryModeCheckIn, "DHBW Mosbach E-Gebäude", "Alexander Auch", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-18-08:16:20"), journal.EntryModeCheckOut, "DHBW Mosbach E-Gebäude", "Alexander Auch", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-18-22:47:49"), journal.EntryModeCheckIn, "\"Tante Gerda\" Mosbach", "Alexander Auch", "Lohrtalweg 10 Mosbach"},
	}

	assert.ElementsMatch(t, expected, analyzer.getRecordsForPerson(testPerson))
}

// In diesem Test wird überprüft, dass der vorherige Datensatz der gleichen Person korrekt gefunden wird.
// Zudem werden die Fehler/Edgefälle geprüft "Kein voriger Datensatz vorhanden (da es der erste Datensatz ist)" und "Übergebener Datensatz existiert nicht in diesem Analyzer"
func TestAnalyzerGetPersonRecordBefore(t *testing.T) {
	analyzer := NewAnalyzer(getTestSet())
	testPerson := Person{"Alexander Auch", "Lohrtalweg 10 Mosbach"}

	records := analyzer.getRecordsForPerson(testPerson)

	// Korrekter Fall
	assert.Equal(t, records[0], *analyzer.getPersonRecordBefore(records[1]))

	// Grenzfall: Erster Datensatz
	assert.Nil(t, analyzer.getPersonRecordBefore(records[0]))

	// Fehlerfall: Keine Vorgänger oder Nachfolger für unbekannte Einträge
	unknownRecord := journal.Entry{
		Timestamp: time.Now(),
		Mode:      journal.EntryModeCheckIn,
		Location:  "ThisLocationDoesNotExist",
		Name:      "Alexander Auch",
		Address:   "Lohrtalweg 10 Mosbach",
	}
	assert.Nil(t, analyzer.getPersonRecordBefore(unknownRecord))
}

// In diesem Test wird überprüft, dass alle Aufenthalte einer Person korrekt aus den Datensätzen gelesen werden.
// Dabei sind in den Testdatensätzen Fehlerquellen enthalten.
func TestGetLocationVisitsForPerson(t *testing.T) {
	analyzer := NewAnalyzer(getTestSet())
	testPerson := Person{"Marie Schreiber", "Koenigstrasse 72 96359 Steinbach"}
	visits := analyzer.GetVisitsForPerson(testPerson)

	expectedVisits := VisitEntries{
		{testPerson, TimeRange{Start: parseTime("2021-11-18-00:00:00"), End: parseTime("2021-11-18-03:11:20")}, "Alte Mälzerei Mosbach"},
		{testPerson, TimeRange{Start: parseTime("2021-11-18-12:04:20"), End: parseTime("2021-11-18-14:26:49")}, "Alte Mälzerei Mosbach"},
		{testPerson, TimeRange{Start: parseTime("2021-11-18-14:26:49").Add(1), End: parseTime("2021-11-18-14:27:49")}, "DHBW Mosbach A-Gebäude"},
		{testPerson, TimeRange{Start: parseTime("2021-11-18-14:28:30"), End: parseTime("2021-11-18-16:07:53")}, "DHBW Mosbach B-Gebäude"},
		{testPerson, TimeRange{Start: parseTime("2021-11-18-16:07:53").Add(1), End: parseTime("2021-11-18-17:47:17")}, "DHBW Mosbach E-Gebäude"},
		{testPerson, TimeRange{Start: parseTime("2021-11-18-18:47:17"), End: parseTime("2021-11-18-21:47:17").Add(-1)}, "DHBW Mosbach Mensa"},
		{testPerson, TimeRange{Start: parseTime("2021-11-18-21:47:17"), End: parseTime("2021-11-18-23:59:59")}, "\"Tante Gerda\" Mosbach"},
	}

	assert.ElementsMatch(t, expectedVisits, visits)
}

// In diesem Test wird überprüft, dass alle Aufenthalte an einem Ort korrekt aus den Datensätzen gelesen werden.
// Dabei sind in den Testdatensätzen Fehlerquellen enthalten.
func TestGetVisitsForLocation(t *testing.T) {
	analyzer := NewAnalyzer(getTestSet())
	testLocation := "DHBW Mosbach B-Gebäude"
	visits := analyzer.GetVisitsForLocation(testLocation)

	expectedVisits := VisitEntries{
		{Person{"Dirk Saller", "Lohrtalweg 10 Mosbach"}, TimeRange{Start: parseTime("2021-11-18-08:17:20"), End: parseTime("2021-11-18-08:21:20").Add(-1)}, "DHBW Mosbach B-Gebäude"},
		{Person{"Marie Schreiber", "Koenigstrasse 72 96359 Steinbach"}, TimeRange{Start: parseTime("2021-11-18-14:28:30"), End: parseTime("2021-11-18-16:07:53")}, "DHBW Mosbach B-Gebäude"},
	}

	assert.ElementsMatch(t, expectedVisits, visits)
}

// In diesem Test wird überprüft, dass alle Kontakte einer Person korrekt aus den Datensätzen gelesen werden.
// Dabei sind in den Testdatensätzen Fehlerquellen enthalten.
func TestGetContactsForPerson(t *testing.T) {
	analyzer := NewAnalyzer(getTestSet())
	testPerson := Person{"Marie Schreiber", "Koenigstrasse 72 96359 Steinbach"}
	contacts := analyzer.GetContactsForPerson(testPerson)

	expectedContacts := PersonContacts{
		Person: testPerson,
		contacts: VisitEntries{
			{Person{"Dirk Saller", "Lohrtalweg 10 Mosbach"}, TimeRange{Start: parseTime("2021-11-18-14:26:49").Add(1), End: parseTime("2021-11-18-14:27:49")}, "DHBW Mosbach A-Gebäude"},
			{Person{"Dirk Saller", "Lohrtalweg 14 Neckarelz"}, TimeRange{Start: parseTime("2021-11-18-18:47:17"), End: parseTime("2021-11-18-21:47:17").Add(-1)}, "DHBW Mosbach Mensa"},
			{Person{"Martina Fuchs", "Hermannstrasse 5 68649 Groß-Rohrheim"}, TimeRange{Start: parseTime("2021-11-18-12:04:20"), End: parseTime("2021-11-18-13:42:52")}, "Alte Mälzerei Mosbach"},
			{Person{"Martina Fuchs", "Hermannstrasse 5 68649 Groß-Rohrheim"}, TimeRange{Start: parseTime("2021-11-18-17:44:30"), End: parseTime("2021-11-18-17:47:17")}, "DHBW Mosbach E-Gebäude"},
			{Person{"Alexander Auch", "Lohrtalweg 10 Mosbach"}, TimeRange{Start: parseTime("2021-11-18-22:47:49"), End: parseTime("2021-11-18-23:59:59")}, "\"Tante Gerda\" Mosbach"},
		},
	}

	assert.Equal(t, expectedContacts.Person, contacts.Person)
	assert.ElementsMatch(t, expectedContacts.contacts, contacts.contacts)
}

// getTestSet stellt einige Testdatensätze zur Verfügung, die bereits darauf getrimmt sind Edge-Cases und Ausnahmefälle abzubilden/auszulösen.
//            Die verwendeten Namen und Personen sind aus dem Lehrkörper des Studiengangs entnommen oder mit https://www.fakenamegenerator.com/gen-random-gr-gr.php generiert worden.
//            Keine Kombination aus Person, Adresse und Ort hat eine konkrete Bedeutung.
//            Die Testdaten enthalten verschiedene Fehler-/Grenzfälle:
//              * Nicht begonnener Aufenthalte
//              * Nicht geschlossener Aufenthalte
//              * Vertauschte Reihenfolge der Datensätze
//              * Datensätze ohne korrekten Modus
//              * ...
func getTestSet() JournalEntries {
	return JournalEntries{
		// Einträge sind sortiert nach Personen: Dadurch direkte Abbildung des Fehlerfalls "Falsche zeitliche Reihenfolge"
		{parseTime("2021-11-18-08:10:20"), journal.EntryModeCheckIn, "DHBW Mosbach E-Gebäude", "Alexander Auch", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-18-08:13:20"), journal.EntryModeCheckOut, "DHBW Mosbach E-Gebäude", "Alexander Auch", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-18-08:14:20"), journal.EntryModeCheckIn, "DHBW Mosbach E-Gebäude", "Alexander Auch", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-18-08:16:20"), journal.EntryModeCheckOut, "DHBW Mosbach E-Gebäude", "Alexander Auch", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-18-22:47:49"), journal.EntryModeCheckIn, "\"Tante Gerda\" Mosbach", "Alexander Auch", "Lohrtalweg 10 Mosbach"},

		{parseTime("2021-11-18-08:00:20"), journal.EntryModeCheckIn, "DHBW Mosbach E-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-18-08:12:20"), journal.EntryModeCheckOut, "DHBW Mosbach E-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-18-08:15:20"), journal.EntryModeCheckIn, "DHBW Mosbach E-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-18-08:16:20"), journal.EntryModeCheckOut, "DHBW Mosbach E-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-18-08:17:20"), journal.EntryModeCheckIn, "DHBW Mosbach B-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},
		// Fehlerfall: CheckIn ohne CheckOut
		{parseTime("2021-11-18-08:21:20"), journal.EntryModeCheckIn, "DHBW Mosbach A-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},

		// Edge-Case: Person mit gleichem Namen, aber anderer Addresse [also andere Person]
		{parseTime("2021-11-18-08:21:25"), journal.EntryModeCheckIn, "DHBW Mosbach Mensa", "Dirk Saller", "Lohrtalweg 14 Neckarelz"},

		// Person für zusätzliche Testfälle bei der Kontaktverfolgung
		{parseTime("2021-11-18-10:14:38"), journal.EntryModeCheckIn, "Alte Mälzerei Mosbach", "Martina Fuchs", "Hermannstrasse 5 68649 Groß-Rohrheim"},
		{parseTime("2021-11-18-13:42:52"), journal.EntryModeCheckOut, "Alte Mälzerei Mosbach", "Martina Fuchs", "Hermannstrasse 5 68649 Groß-Rohrheim"},
		{parseTime("2021-11-18-17:44:30"), journal.EntryModeCheckIn, "DHBW Mosbach E-Gebäude", "Martina Fuchs", "Hermannstrasse 5 68649 Groß-Rohrheim"},
		{parseTime("2021-11-18-20:34:52"), journal.EntryModeCheckOut, "DHBW Mosbach E-Gebäude", "Martina Fuchs", "Hermannstrasse 5 68649 Groß-Rohrheim"},

		// Fehlerfall: CheckOut ohne pasenden CheckIn als erste Aktion des Tages (z.B. aufgrund eines Aufenthaltes über die Tagesgrenze)
		{parseTime("2021-11-18-03:11:20"), journal.EntryModeCheckOut, "Alte Mälzerei Mosbach", "Marie Schreiber", "Koenigstrasse 72 96359 Steinbach"},
		{parseTime("2021-11-18-12:04:20"), journal.EntryModeCheckIn, "Alte Mälzerei Mosbach", "Marie Schreiber", "Koenigstrasse 72 96359 Steinbach"},
		// Fehlerfall: Wiederholter CheckIn am gleichen Ort
		{parseTime("2021-11-18-12:34:20"), journal.EntryModeCheckIn, "Alte Mälzerei Mosbach", "Marie Schreiber", "Koenigstrasse 72 96359 Steinbach"},
		{parseTime("2021-11-18-14:26:49"), journal.EntryModeCheckOut, "Alte Mälzerei Mosbach", "Marie Schreiber", "Koenigstrasse 72 96359 Steinbach"},
		// Fehlerfall: CheckOut ohne vorigen CheckIn
		{parseTime("2021-11-18-14:27:49"), journal.EntryModeCheckOut, "DHBW Mosbach A-Gebäude", "Marie Schreiber", "Koenigstrasse 72 96359 Steinbach"},
		{parseTime("2021-11-18-14:28:30"), journal.EntryModeCheckIn, "DHBW Mosbach B-Gebäude", "Marie Schreiber", "Koenigstrasse 72 96359 Steinbach"},
		// Fehlerfall: Event mit ungültigem Modus -> Darf nicht ausgewertet werden
		{parseTime("2021-11-18-15:10:22"), "TRANSIT", "DHBW Mosbach E-Gebäude", "Marie Schreiber", "Koenigstrasse 72 96359 Steinbach"},
		// Fehlerfall: CheckOut für anderen Ort als der CheckIn
		{parseTime("2021-11-18-17:47:17"), journal.EntryModeCheckOut, "DHBW Mosbach E-Gebäude", "Marie Schreiber", "Koenigstrasse 72 96359 Steinbach"},
		{parseTime("2021-11-18-18:47:17"), journal.EntryModeCheckIn, "DHBW Mosbach Mensa", "Marie Schreiber", "Koenigstrasse 72 96359 Steinbach"},
		// Fehlerfall: CheckIn ohne vorigen CheckOut
		{parseTime("2021-11-18-21:47:17"), journal.EntryModeCheckIn, "\"Tante Gerda\" Mosbach", "Marie Schreiber", "Koenigstrasse 72 96359 Steinbach"},
		// Edgecase: Abschließender CheckIn ohne CheckOut (z.B. weil dieser erst am nächsten Tag erfolgt)
	}
}

// Hilfsmethode um eine einfachere Angabe von Zeiten zu ermöglichen
func parseTime(timestring string) time.Time {
	tm, err := time.Parse("2006-01-02-15:04:05", timestring)
	if err != nil {
		return time.UnixMicro(0)
	}
	return tm
}
