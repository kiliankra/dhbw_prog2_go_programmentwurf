/*
 * 2627585, 3110300, 2858031
 */

package analysis

import (
	"github.com/stretchr/testify/assert"
	"prog2_go_programmentwurf/src/journal"
	"sort"
	"testing"
)

func TestIsCheckIn(t *testing.T) {
	checkInRecord := journal.Entry{Mode: journal.EntryModeCheckIn}
	checkInRecord2 := journal.Entry{Mode: "CI"}
	checkOutRecord := journal.Entry{Mode: journal.EntryModeCheckOut}
	invalidRecord := journal.Entry{Mode: "NAY"}

	assert.True(t, IsCheckIn(&checkInRecord))
	assert.True(t, IsCheckIn(&checkInRecord2))
	assert.False(t, IsCheckIn(&checkOutRecord))
	assert.False(t, IsCheckIn(&invalidRecord))
}

func TestIsCheckOut(t *testing.T) {
	checkOutRecord := journal.Entry{Mode: journal.EntryModeCheckOut}
	checkOutRecord2 := journal.Entry{Mode: "CO"}
	checkInRecord := journal.Entry{Mode: journal.EntryModeCheckIn}
	invalidRecord := journal.Entry{Mode: "NAY"}

	assert.True(t, IsCheckOut(&checkOutRecord))
	assert.True(t, IsCheckOut(&checkOutRecord2))
	assert.False(t, IsCheckOut(&checkInRecord))
	assert.False(t, IsCheckOut(&invalidRecord))
}

func TestJournalEntriesSortable(t *testing.T) {
	testEntries := JournalEntries{
		{parseTime("2021-11-19-09:00:20"), journal.EntryModeCheckIn, "DHBW Mosbach E-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-19-08:12:20"), journal.EntryModeCheckOut, "DHBW Mosbach E-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-19-10:12:20"), journal.EntryModeCheckOut, "DHBW Mosbach E-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-19-08:10:20"), journal.EntryModeCheckIn, "DHBW Mosbach E-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},
	}
	sortedTestEntries := JournalEntries{
		{parseTime("2021-11-19-08:10:20"), journal.EntryModeCheckIn, "DHBW Mosbach E-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-19-08:12:20"), journal.EntryModeCheckOut, "DHBW Mosbach E-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-19-09:00:20"), journal.EntryModeCheckIn, "DHBW Mosbach E-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-19-10:12:20"), journal.EntryModeCheckOut, "DHBW Mosbach E-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},
	}

	// Do sorting
	sort.Sort(testEntries)

	assert.Equal(t, sortedTestEntries, testEntries)
}

func TestTimeRange_Contains(t *testing.T) {
	tm := TimeRange{
		Start: parseTime("2021-11-19-08:10:20"),
		End:   parseTime("2021-11-19-10:12:20"),
	}

	assert.True(t, tm.Contains(parseTime("2021-11-19-09:32:17")))
	assert.True(t, tm.Contains(parseTime("2021-11-19-08:10:20")))
	assert.True(t, tm.Contains(parseTime("2021-11-19-10:12:20")))
	assert.False(t, tm.Contains(parseTime("2021-11-19-08:10:20").Add(-1)))
	assert.False(t, tm.Contains(parseTime("2021-11-19-10:12:20").Add(1)))
}

func TestTimeRange_ContainsRange(t *testing.T) {
	tm := TimeRange{
		Start: parseTime("2021-11-19-08:10:20"),
		End:   parseTime("2021-11-19-10:12:20"),
	}

	testRange := TimeRange{parseTime("2021-11-19-08:10:20"), parseTime("2021-11-19-10:12:20")}
	assert.True(t, tm.ContainsRange(testRange))
	testRange = TimeRange{parseTime("2021-11-19-09:15:20"), parseTime("2021-11-19-10:12:20")}
	assert.True(t, tm.ContainsRange(testRange))
	testRange = TimeRange{parseTime("2021-11-19-08:10:20"), parseTime("2021-11-19-09:15:20")}
	assert.True(t, tm.ContainsRange(testRange))
	testRange = TimeRange{parseTime("2021-11-19-08:10:20").Add(-1), parseTime("2021-11-19-10:12:20")}
	assert.False(t, tm.ContainsRange(testRange))
	testRange = TimeRange{parseTime("2021-11-19-08:10:20"), parseTime("2021-11-19-10:12:20").Add(1)}
	assert.False(t, tm.ContainsRange(testRange))
	testRange = TimeRange{parseTime("2021-11-19-08:10:20").Add(-1), parseTime("2021-11-19-10:12:20").Add(1)}
	assert.False(t, tm.ContainsRange(testRange))
}
