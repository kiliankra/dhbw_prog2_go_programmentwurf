/*
 * 2627585, 3110300, 2858031
 */

package analysis

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strings"
	"time"
)

// Exporter stellt Methoden zur Ausgabe von Analyseergebnissen bereit
type Exporter interface {
	ExportPersonVisits(visits VisitEntries)
	ExportLocationVisits(visits VisitEntries)
	ExportPersonContacts(contactData PersonContacts)
}

// NewExporter Erstellt einen neuen Exporter;
// targetType  gibt die Implementierung des Exporters an ("csv", "stdout");
// file gibt   die Zieldatei an (dies ist nur für CSV-Exporter relevant, in anderen Fällen darf dies nil sein)
func NewExporter(targetType string, file *os.File) Exporter {
	var exporter Exporter
	switch strings.ToLower(targetType) {
	case "csv":
		exporter = csvExporter{exportFile: file}
	case "stdout":
		// Does not need a file
		exporter = stdoutExporter{}
	default:
		log.Println("ERROR: Der angegebene Exporttyp existiert nicht.")
		return nil
	}

	return exporter
}

// Timestamp-Format das in CSVs von Microsoft Excel verstanden wird
const csvTimestampFormat = "2006-01-02 15:04:05"

// Implementierung für den CSV-Export
type csvExporter struct {
	exportFile *os.File
}

func (exporter csvExporter) ExportPersonVisits(visits VisitEntries) {
	csvWriter := csv.NewWriter(exporter.exportFile)
	defer csvWriter.Flush()

	// Ggf. Header für die einfachere Verarbeitung in Excel schreiben
	if !exporter.hasFileAlreadyData() {
		err := csvWriter.Write([]string{"Ort", "CheckIn", "CheckOut"})
		if err != nil {
			log.Panicln("ERROR: Fehler beim Export: ", err)
		}
	}

	for _, visit := range visits {
		err := csvWriter.Write([]string{
			visit.Location,
			visit.Start.Format(csvTimestampFormat),
			visit.End.Format(csvTimestampFormat),
		})
		if err != nil {
			log.Panicln("ERROR: Fehler beim Export: ", err)
		}
	}
}

func (exporter csvExporter) ExportLocationVisits(visits VisitEntries) {
	csvWriter := csv.NewWriter(exporter.exportFile)
	defer csvWriter.Flush()

	// Ggf. Header für die einfachere Verarbeitung in Excel schreiben
	if !exporter.hasFileAlreadyData() {
		err := csvWriter.Write([]string{"Name", "Adresse", "CheckIn", "CheckOut"})
		if err != nil {
			log.Panicln("ERROR: Fehler beim Export: ", err)
		}
	}

	for _, visit := range visits {
		err := csvWriter.Write([]string{
			visit.Name,
			visit.Address,
			visit.Start.Format(csvTimestampFormat),
			visit.End.Format(csvTimestampFormat),
		})
		if err != nil {
			log.Panicln("ERROR: Fehler beim Export: ", err)
		}
	}
}

func (exporter csvExporter) ExportPersonContacts(contactData PersonContacts) {
	csvWriter := csv.NewWriter(exporter.exportFile)
	defer csvWriter.Flush()

	// Ggf. Header für die einfachere Verarbeitung in Excel schreiben
	if !exporter.hasFileAlreadyData() {
		err := csvWriter.Write([]string{"Name", "Adresse", "Ort", "CheckIn", "CheckOut"})
		if err != nil {
			log.Panicln("ERROR: Fehler beim Export: ", err)
		}
	}

	for _, contact := range contactData.contacts {
		err := csvWriter.Write([]string{
			contact.Name,
			contact.Address,
			contact.Location,
			contact.Start.Format(csvTimestampFormat),
			contact.End.Format(csvTimestampFormat),
		})
		if err != nil {
			log.Panicln("ERROR: Fehler beim Export: ", err)
		}
	}
}

// Hilfsmethode zur Prüfung, ob die Zieldatei leer ist oder nicht
func (exporter csvExporter) hasFileAlreadyData() bool {
	if exporter.exportFile == nil {
		panic("export file should not be nil")
	}

	info, err := exporter.exportFile.Stat()
	if err != nil {
		// File might not exist
		return false
	}
	return info.Size() > 0
}

// Implementierung für die lesbare Ausgabe auf dem Terminal
type stdoutExporter struct{}

// Lesbarer Timestamp
const stdoutTimestampFormat = "02.01.2006 15:04:05 Uhr"

func (exporter stdoutExporter) ExportPersonVisits(visits VisitEntries) {
	if len(visits) > 0 {
		logString := fmt.Sprintf(
			"Aufenthalte am %v\n"+
				"Personendaten:\n"+
				"  Name: %v\n"+
				"  Adresse: %v\n\n",
			visits[0].Start.Format("02.01.2006"),
			visits[0].Name, visits[0].Address)
		for _, visit := range visits {
			logString += fmt.Sprintf("* %v von %v bis %v\n", visit.Location, visit.Start.Format(stdoutTimestampFormat), visit.End.Format(stdoutTimestampFormat))
		}
		log.Print(logString)
	} else {
		log.Println("Es konnten keine Aufenthalte zu dieser Person gefunden werden.")
	}
}

func (exporter stdoutExporter) ExportLocationVisits(visits VisitEntries) {
	if len(visits) > 0 {
		logString := fmt.Sprintf(
			"Aufenthalte am %v bei/in %v\n\n",
			visits[0].Start.Format("02.01.2006"),
			visits[0].Location)
		for _, visit := range visits {
			logString += fmt.Sprintf("* %v (Adresse: %v) von %v bis %v\n", visit.Name, visit.Address, visit.Start.Format(stdoutTimestampFormat), visit.End.Format(stdoutTimestampFormat))
		}
		log.Print(logString)
	} else {
		log.Println("Es konnten keine Aufenthalte zu diesem Ort gefunden werden.")
	}
}

func (exporter stdoutExporter) ExportPersonContacts(contactData PersonContacts) {
	logString := fmt.Sprintf("Mögliche Kontakte von %v (Adresse: %v)\n", contactData.Name, contactData.Address)

	categorized := categorizeByPerson(contactData.contacts)
	for contactPerson, contacts := range categorized {
		logString += fmt.Sprintf("\n* Kontakte mit %v (Gesamtdauer: %v)(Kontaktadresse: %v)\n",
			contactPerson.Name, accumulateDuration(contacts).String(), contactPerson.Address)
		for _, contact := range contacts {
			logString += fmt.Sprintf("  - Von %v bis %v bei/in %v\n",
				contact.Start.Format(stdoutTimestampFormat), contact.End.Format(stdoutTimestampFormat), contact.Location)
		}
	}

	log.Print(logString)
}

func categorizeByPerson(visits VisitEntries) map[Person]VisitEntries {
	toReturn := make(map[Person]VisitEntries)

	for _, visit := range visits {
		if list, ok := toReturn[visit.Person]; ok {
			toReturn[visit.Person] = append(list, visit)
		} else {
			toReturn[visit.Person] = VisitEntries{visit}
		}
	}

	return toReturn
}

func accumulateDuration(visits VisitEntries) time.Duration {
	duration := time.Duration(0)
	for _, visit := range visits {
		duration += visit.End.Sub(visit.Start)
	}
	return duration
}
