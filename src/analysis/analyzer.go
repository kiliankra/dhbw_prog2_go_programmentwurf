/*
 * 2627585, 3110300, 2858031
 */

package analysis

import (
	"log"
	"prog2_go_programmentwurf/src/journal"
	"sort"
	"time"
)

// Analyzer stellt Methoden zur Analyse eines Journals (bzw. JournalEntries) bereit.
type Analyzer interface {
	// GetPeople liefert eine Liste aller Personen, die im Datensatz enthalten sind
	GetPeople() []Person
	// GetLocations liefert eine Liste aller Orte, die im Datensatz enthalten sind
	GetLocations() []string

	// Methoden zur Durchführung der eigentlichen Analyse
	GetVisitsForPerson(person Person) VisitEntries
	GetVisitsForLocation(location string) VisitEntries
	GetContactsForPerson(person Person) PersonContacts

	// Interne Hilfsmethoden (nicht exportiert)
	getRecords() JournalEntries
	getPersonRecordBefore(record journal.Entry) *journal.Entry
	getRecordsForPerson(person Person) JournalEntries
	getContactsForVisit(visit VisitEntry) VisitEntries
}

// Struktur zur Implementierung der Analyzer-Schnittstelle
type analyzerImpl struct {
	// Journaldaten zur Analyse
	records JournalEntries

	// Cache zur Vermeidung der wiederholten Bestimmung von Daten
	cachePersonVisits   map[Person]VisitEntries
	cacheLocationVisits map[string]VisitEntries
}

// NewAnalyzer sortiert die JournalEntries nach dem angegebenen Zeitstempel und erzeugt eine Analyzer-Instanz
func NewAnalyzer(records JournalEntries) Analyzer {
	sort.Sort(records)
	return &analyzerImpl{records: records, cachePersonVisits: make(map[Person]VisitEntries), cacheLocationVisits: make(map[string]VisitEntries)}
}

func (analyzer *analyzerImpl) GetPeople() []Person {
	people := make([]Person, 0, 10)

	for _, record := range analyzer.records {
		person := Person{Name: record.Name, Address: record.Address}
		if !containsPerson(people, person) {
			people = append(people, person)
		}
	}

	return people
}

func (analyzer *analyzerImpl) GetLocations() []string {
	locations := make([]string, 0, 10)

	for _, record := range analyzer.records {
		if !containsString(locations, record.Location) {
			locations = append(locations, record.Location)
		}
	}

	return locations
}

//GetVisitsForPerson erstellt die Aufenthaltsdatensätze für eine Person und führt gegebenenfalls Fehlerkorrekturen durch
func (analyzer *analyzerImpl) GetVisitsForPerson(person Person) VisitEntries {
	if cached, ok := analyzer.cachePersonVisits[person]; ok {
		return cached
	}

	personRecords := analyzer.getRecordsForPerson(person)
	// Falls alle Journal-Einträge korrekt sind, gibt es für jeden Aufenthalt genau 2 Einträge
	visits := make(VisitEntries, 0, len(personRecords)/2)

	var currentVisit *VisitEntry = nil
	for _, record := range personRecords {
		// Check-In registriert
		if IsCheckIn(&record) {
			if currentVisit != nil {
				if currentVisit.Location == record.Location {
					log.Println("WARN: Es wurde ein wiederholter CheckIn für den gleichen Ort gefunden. Der wiederholte CheckIn wird ignoriert.")
				} else {
					log.Println("WARN: Es wurde ein CheckIn ohne zugehörigen CheckOut gefunden. Der Zeitpunkt des nächsten CheckIn-Eintrags wird als CheckOut verwendet.")
					// Der vorige Aufenthalt wird minimal vor Beginn des aktuellen beendet, um eine Überlappung zu vermeiden
					currentVisit.TimeRange.End = record.Timestamp.Add(-1)
					visits = append(visits, *currentVisit)
					currentVisit = nil
					// Der alte Aufenthalt ist nun beendet -> [Explicit Fallthrough] Weiter mit dem Standardverfahren
				}
			}
			if currentVisit == nil {
				// Gültiger Fall
				visit := VisitEntry{
					Person:    Person{record.Name, record.Address},
					TimeRange: TimeRange{Start: record.Timestamp},
					Location:  record.Location,
				}
				currentVisit = &visit
			}
			// Check-Out registriert
		} else if IsCheckOut(&record) {
			if currentVisit == nil {
				log.Println("WARN: Es wurde ein CheckOut ohne zugehörigen CheckIn gefunden. Der Zeitpunkt des letzten Eintrags wird als CheckIn verwendet.")
				previousRecord := analyzer.getPersonRecordBefore(record)
				var fromTime time.Time
				if previousRecord != nil {
					// Beginne den korrigierten Aufenthalt kurz nach Ende des zuletz aufgezeichneten Aufenthalts
					fromTime = previousRecord.Timestamp.Add(1)
				} else {
					log.Println("WARN: Es wurde kein vorheriger Eintrag gefunden. Der Beginn des Tages wird als Ankunftszeit angenommen.")
					tm := record.Timestamp
					fromTime = time.Date(tm.Year(), tm.Month(), tm.Day(), 0, 0, 0, 0, tm.Location())
				}
				visit := VisitEntry{
					Person:    Person{record.Name, record.Address},
					TimeRange: TimeRange{fromTime, record.Timestamp},
					Location:  record.Location,
				}
				visits = append(visits, visit)
			} else if currentVisit.Location != record.Location {
				log.Println("WARN: Es wurde ein CheckOut gefunden, der nicht zum aktuellen CheckIn passt. Die Mitte des Zeitraums wird als Zeitpunkt des Wechsels angenommen.")
				switchTimeUnix := (currentVisit.TimeRange.Start.Unix() + record.Timestamp.Unix()) / 2
				switchTime := time.Unix(switchTimeUnix, 0).In(time.UTC)

				currentVisit.TimeRange.End = switchTime
				visits = append(visits, *currentVisit)
				currentVisit = nil
				visit := VisitEntry{
					Person:    Person{record.Name, record.Address},
					TimeRange: TimeRange{switchTime.Add(1), record.Timestamp},
					Location:  record.Location,
				}
				visits = append(visits, visit)
			} else {
				// Gültiger Fall
				currentVisit.TimeRange.End = record.Timestamp
				visits = append(visits, *currentVisit)
				currentVisit = nil
			}
		} else {
			log.Println("WARN: Es wurde ein ungültiger Eintrag gefunden, der weder Check-In noch Check-Out ist: ", record)
		}
	}

	// Der letzte Aufenthalt wurde nicht korrekt beendet.
	if currentVisit != nil {
		log.Println("WARN: CheckIn am Ende ohne CheckOut gefunden. Als Endzeitpunkt wird das Ende des aktuellen Tages angenommen.")
		tm := currentVisit.TimeRange.Start
		untilTime := time.Date(tm.Year(), tm.Month(), tm.Day(), 23, 59, 59, 0, tm.Location())

		currentVisit.TimeRange.End = untilTime
		visits = append(visits, *currentVisit)
	}

	// Abspeichern im Cache, um eine wiederholte Bestimmung der Besuche zu vermeiden.
	analyzer.cachePersonVisits[person] = visits

	return visits
}

func (analyzer *analyzerImpl) GetVisitsForLocation(location string) VisitEntries {
	// Überprüfen, ob die Visits für den Ort bereits im Cache vorhanden sind
	if cached, ok := analyzer.cacheLocationVisits[location]; ok {
		return cached
	}

	// Um Fehler im Jounal zu korrigieren/zu kompensieren genügt es nicht die Records an einem Ort zu betrachten.
	// Zudem sollte der Code für die Fehlerkorrektur nur einmal implementiert werden (DRY).
	// Daher werden die Visits für alle Personen bestimmt (hier sind die Korrekturen einfacher) und dann die Aufenthalte aller Personen nach dem Ort gefiltert.
	// Dies ist aufgrund des Cachings kein Performanceproblem.
	toReturn := make(VisitEntries, 0)
	people := analyzer.GetPeople()

	for _, person := range people {
		personVisits := analyzer.GetVisitsForPerson(person)
		for _, visit := range personVisits {
			if visit.Location == location {
				toReturn = append(toReturn, visit)
			}
		}
	}

	sort.Sort(toReturn)

	// Abspeichern im Cache, um eine wiederholte Bestimmung der Besuche zu vermeiden.
	analyzer.cacheLocationVisits[location] = toReturn
	return toReturn
}

func (analyzer *analyzerImpl) GetContactsForPerson(person Person) PersonContacts {
	// Die Kontakte einer Person werden bestimmt indem für jeden Aufenthalt einer Person einzeln die Kontakte bestimmt werden
	visits := analyzer.GetVisitsForPerson(person)
	contacts := make(VisitEntries, 0)

	for _, visit := range visits {
		contacts = append(contacts, analyzer.getContactsForVisit(visit)...)
	}

	return PersonContacts{person, contacts}
}

func (analyzer *analyzerImpl) getRecords() JournalEntries {
	return analyzer.records
}

func (analyzer *analyzerImpl) getPersonRecordBefore(record journal.Entry) *journal.Entry {
	person := Person{Name: record.Name, Address: record.Address}
	personRecords := analyzer.getRecordsForPerson(person)
	currentIndex := -1

	for index, persRecord := range personRecords {
		if persRecord == record {
			currentIndex = index
			break
		}
	}

	// Der ursprüngliche Entry existiert nicht (dieser Fall sollte nicht auftreten)
	if currentIndex == -1 {
		return nil
	}

	// Der ursprüngliche Entry ist die erste aufgezeichnete Aktion, daher existiert kein voriger Eintrag
	if currentIndex == 0 {
		return nil
	}

	return &(personRecords[currentIndex-1])
}

func (analyzer *analyzerImpl) getRecordsForPerson(person Person) JournalEntries {
	records := make(JournalEntries, 0, 10)

	for _, record := range analyzer.records {
		if record.Name == person.Name && record.Address == person.Address {
			records = append(records, record)
		}
	}

	return records
}

func (analyzer *analyzerImpl) getContactsForVisit(visit VisitEntry) VisitEntries {
	// Die Bestimmung der Kontakte erfolgt anhand der anderen Anwesenheiten an dem Ort.
	locationVisits := analyzer.GetVisitsForLocation(visit.Location)
	contacts := make(VisitEntries, 0)

	for _, locationVisit := range locationVisits {
		// Alle Anwesenheiten, die zur gleichen Person gehören müssen nicht als Kontakte betrachtet werden
		if locationVisit.Person == visit.Person {
			continue
		}

		// Eine andere Anwesenheit liegt vollständig innerhalb der Dauer der zu prüfenden Anwesenheit
		if visit.ContainsRange(locationVisit.TimeRange) {
			contacts = append(contacts, locationVisit)
			continue
		}

		// Eine andere Anwesenheit begann innerhalb der Dauer der zu prüfenden Anwesenheit und dauerte länger an
		if visit.Contains(locationVisit.End) {
			locationVisit.Start = visit.Start
			contacts = append(contacts, locationVisit)
			continue
		}

		// Eine andere Anwesenheit endete innerhalb der Dauer der zu prüfenden Anwesenheit und begann davor
		if visit.Contains(locationVisit.Start) {
			locationVisit.End = visit.End
			contacts = append(contacts, locationVisit)
			continue
		}

		// Eine andere Anwesenheit begann bevor der zu prüfenden Anwesenheit und endete danach
		if locationVisit.ContainsRange(visit.TimeRange) {
			locationVisit.Start = visit.Start
			locationVisit.End = visit.End
			contacts = append(contacts, locationVisit)
			continue
		}
	}

	return contacts
}

// Hilfsmethode um zu prüfen, ob eine Slice einen String enthält, um doppelte Einträge zu vermeiden
func containsString(slice []string, str string) bool {
	for _, a := range slice {
		if a == str {
			return true
		}
	}
	return false
}

// Hilfsmethode um zu prüfen, ob eine Slice eine Person enthält, um doppelte Einträge zu vermeiden
func containsPerson(slice []Person, pers Person) bool {
	for _, a := range slice {
		if a == pers {
			return true
		}
	}
	return false
}
