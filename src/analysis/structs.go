/*
 * 2627585, 3110300, 2858031
 */

package analysis

import (
	"prog2_go_programmentwurf/src/journal"
	"time"
)

type Person struct {
	Name    string
	Address string
}

type TimeRange struct {
	Start time.Time
	End   time.Time
}

func (t TimeRange) Contains(tm time.Time) bool {
	return t.Start == tm || t.End == tm ||
		(t.Start.Before(tm) && t.End.After(tm))
}

func (t TimeRange) ContainsRange(other TimeRange) bool {
	return t.Contains(other.Start) && t.Contains(other.End)
}

type VisitEntry struct {
	Person
	TimeRange
	Location string
}

type PersonContacts struct {
	Person
	contacts []VisitEntry
}

func IsCheckIn(record *journal.Entry) bool {
	return record != nil && (*record).Mode == journal.EntryModeCheckIn
}

func IsCheckOut(record *journal.Entry) bool {
	return record != nil && (*record).Mode == journal.EntryModeCheckOut
}

// JournalEntries ist ein Alias, um eine Vereinfachte Schreibweise für ein Slice of Entry.
//                Zudem wird dieses Alias genutzt, um Slices dieses Typen sortierbar zu machen, indem sort.Interface implementiert wird.
type JournalEntries []journal.Entry

func (entries JournalEntries) Len() int {
	return len(entries)
}

// Less ist die Vergleichsfunktion für Elemente an den angegebenen Indizes.
//      Für JournalEntries werden die Elemente anhand ihres Timestamps sortiert.
func (entries JournalEntries) Less(i, j int) bool {
	return entries[i].Timestamp.Before(entries[j].Timestamp)
}

func (entries JournalEntries) Swap(i, j int) {
	entries[i], entries[j] = entries[j], entries[i]
}

// VisitEntries ist ein Alias, um eine Vereinfachte Schreibweise für ein Slice of VisitEntry.
//              Zudem wird dieses Alias genutzt, um Slices dieses Typen sortierbar zu machen, indem sort.Interface implementiert wird.
type VisitEntries []VisitEntry

func (entries VisitEntries) Len() int {
	return len(entries)
}

// Less ist die Vergleichsfunktion für Elemente an den angegebenen Indizes.
//      Für VisitEntries werden die Elemente anhand ihres Startzeitpunktes sortiert.
func (entries VisitEntries) Less(i, j int) bool {
	return entries[i].TimeRange.Start.Before(entries[j].TimeRange.Start)
}

func (entries VisitEntries) Swap(i, j int) {
	entries[i], entries[j] = entries[j], entries[i]
}
