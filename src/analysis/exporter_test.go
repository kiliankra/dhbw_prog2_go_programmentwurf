/*
 * 2627585, 3110300, 2858031
 */

package analysis

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"log"
	"os"
	"testing"
	"time"
)

// In diesem Test wird überprüft, dass die Initialisierung der Exporter-Objekte funktioniert wie erwartet.
func TestNewExporter(t *testing.T) {
	// Prüfe, dass ein CSV-Exporter erstellt wird
	csvExport := NewExporter("csv", nil)
	assert.IsType(t, csvExporter{}, csvExport)
	csvExport = NewExporter("cSV", nil)
	assert.IsType(t, csvExporter{}, csvExport)

	// Prüfe, dass ein StdOut-Exporter erstellt wird
	stdoutExport := NewExporter("stdout", nil)
	assert.IsType(t, stdoutExporter{}, stdoutExport)
	stdoutExport = NewExporter("STdOuT", nil)
	assert.IsType(t, stdoutExporter{}, stdoutExport)

	// Prüfe, dass kein Exporter erstellt wird bei einer ungültigen Eingabe
	assert.Nil(t, NewExporter("notatype", nil))
}

// In diesem Test wird überprüft, dass ein Export mit einem CSV-Exporter nicht möglich ist, wenn keine Datei bei der Initialisierung gesetzt wurde
func TestCsvExporterPanicsWithoutFile(t *testing.T) {
	exporter := NewExporter("csv", nil)

	assert.Panics(t, func() {
		exporter.ExportPersonContacts(PersonContacts{
			Person:   Person{},
			contacts: VisitEntries{{}, {}},
		})
	})
	assert.Panics(t, func() {
		exporter.ExportPersonVisits(VisitEntries{{}, {}})
	})
	assert.Panics(t, func() {
		exporter.ExportLocationVisits(VisitEntries{{}, {}})
	})
}

// In diesem Test wird überprüft, dass die Kontakte in der exportierten CSV-Datei mit den erwarteten Werten übereinstimmen
func TestCsvExporter_ExportPersonContacts(t *testing.T) {
	output := captureTestFileOutput(func(file *os.File) {
		exporter := NewExporter("csv", file)

		contacts := PersonContacts{
			Person: Person{"Theo Est", "Fontane-Weg 14a 78325 Niemandsland"},
			contacts: VisitEntries{
				{Person{"Vanessa Gärtner", "Neuer Jungfernstieg 18 84155 Bodenkirchen"}, TimeRange{parseTime("2021-12-24-16:26:49"), parseTime("2021-12-24-17:02:37")}, "Pfarrkirche Unterobertalspitze"},
				{Person{"Lucas Pfeifer", "Eschenweg 23 07309 Saalfeld"}, TimeRange{parseTime("2021-12-24-16:20:32"), parseTime("2021-12-24-17:10:56")}, "Pfarrkirche Unterobertalspitze"},
			},
		}

		exporter.ExportPersonContacts(contacts)
	})

	expected :=
		"Name,Adresse,Ort,CheckIn,CheckOut\n" +
			"Vanessa Gärtner,Neuer Jungfernstieg 18 84155 Bodenkirchen,Pfarrkirche Unterobertalspitze,2021-12-24 16:26:49,2021-12-24 17:02:37\n" +
			"Lucas Pfeifer,Eschenweg 23 07309 Saalfeld,Pfarrkirche Unterobertalspitze,2021-12-24 16:20:32,2021-12-24 17:10:56\n"
	assert.Equal(t, expected, output)
}

// In diesem Test wird überprüft, dass die Aufenthaltsorte einer Person in der exportierten CSV-Datei mit den erwarteten Werten übereinstimmen
func TestCsvExporter_ExportPersonVisits(t *testing.T) {
	output := captureTestFileOutput(func(file *os.File) {
		exporter := NewExporter("csv", file)

		visits := VisitEntries{
			{Person{"Vanessa Gärtner", "Neuer Jungfernstieg 18 84155 Bodenkirchen"}, TimeRange{parseTime("2021-12-24-16:26:49"), parseTime("2021-12-24-17:02:37")}, "Pfarrkirche Unterobertalspitze"},
			{Person{"Vanessa Gärtner", "Neuer Jungfernstieg 18 84155 Bodenkirchen"}, TimeRange{parseTime("2021-12-24-17:20:32"), parseTime("2021-12-24-17:40:56")}, "Weihnachtsmarkt Unterobertalspitze"},
		}

		exporter.ExportPersonVisits(visits)
	})

	expected :=
		"Ort,CheckIn,CheckOut\n" +
			"Pfarrkirche Unterobertalspitze,2021-12-24 16:26:49,2021-12-24 17:02:37\n" +
			"Weihnachtsmarkt Unterobertalspitze,2021-12-24 17:20:32,2021-12-24 17:40:56\n"
	assert.Equal(t, expected, output)
}

// In diesem Test wird überprüft, dass die Aufenthalte an einem Ort in der exportierten CSV-Datei mit den erwarteten Werten übereinstimmen
func TestCsvExporter_ExportLocationVisits(t *testing.T) {
	output := captureTestFileOutput(func(file *os.File) {
		exporter := NewExporter("csv", file)

		visits := VisitEntries{
			{Person{"Vanessa Gärtner", "Neuer Jungfernstieg 18 84155 Bodenkirchen"}, TimeRange{parseTime("2021-12-24-16:26:49"), parseTime("2021-12-24-17:02:37")}, "Pfarrkirche Unterobertalspitze"},
			{Person{"Lucas Pfeifer", "Eschenweg 23 07309 Saalfeld"}, TimeRange{parseTime("2021-12-24-16:20:32"), parseTime("2021-12-24-17:10:56")}, "Pfarrkirche Unterobertalspitze"},
		}

		exporter.ExportLocationVisits(visits)
	})

	expected :=
		"Name,Adresse,CheckIn,CheckOut\n" +
			"Vanessa Gärtner,Neuer Jungfernstieg 18 84155 Bodenkirchen,2021-12-24 16:26:49,2021-12-24 17:02:37\n" +
			"Lucas Pfeifer,Eschenweg 23 07309 Saalfeld,2021-12-24 16:20:32,2021-12-24 17:10:56\n"
	assert.Equal(t, expected, output)
}

// In diesem Test wird überprüft, dass die ausgegebenen Kontakte mit den erwarteten Werten übereinstimmen
func TestStdoutExporter_ExportPersonContacts(t *testing.T) {
	output := captureLogOutput(func() {
		exporter := NewExporter("stdout", nil)

		contacts := PersonContacts{
			Person: Person{"Theo Est", "Fontane-Weg 14a 78325 Niemandsland"},
			contacts: VisitEntries{
				{Person{"Vanessa Gärtner", "Neuer Jungfernstieg 18 84155 Bodenkirchen"}, TimeRange{parseTime("2021-12-24-16:26:49"), parseTime("2021-12-24-17:02:37")}, "Pfarrkirche Unterobertalspitze"},
				{Person{"Lucas Pfeifer", "Eschenweg 23 07309 Saalfeld"}, TimeRange{parseTime("2021-12-24-16:20:32"), parseTime("2021-12-24-17:10:56")}, "Pfarrkirche Unterobertalspitze"},
			},
		}

		exporter.ExportPersonContacts(contacts)
	})

	expectedHeader := "Mögliche Kontakte von Theo Est (Adresse: Fontane-Weg 14a 78325 Niemandsland)\n"
	expectedContact1 := "\n" +
		"* Kontakte mit Vanessa Gärtner (Gesamtdauer: 35m48s)(Kontaktadresse: Neuer Jungfernstieg 18 84155 Bodenkirchen)\n" +
		"  - Von 24.12.2021 16:26:49 Uhr bis 24.12.2021 17:02:37 Uhr bei/in Pfarrkirche Unterobertalspitze\n"
	expectedContact2 := "\n" +
		"* Kontakte mit Lucas Pfeifer (Gesamtdauer: 50m24s)(Kontaktadresse: Eschenweg 23 07309 Saalfeld)\n" +
		"  - Von 24.12.2021 16:20:32 Uhr bis 24.12.2021 17:10:56 Uhr bei/in Pfarrkirche Unterobertalspitze\n"

	// Die Reihenfolge in der die Personen ausgegeben werden ist nicht relevant, daher wird nur geprüft, dass alle Bestandteile der Ausgabe existieren
	assert.Contains(t, output, expectedHeader)
	assert.Contains(t, output, expectedContact1)
	assert.Contains(t, output, expectedContact2)
}

// In diesem Test wird überprüft, dass die ausgegebenen Aufenthaltsorte einer Person mit den erwarteten Werten übereinstimmen
func TestStdoutExporter_ExportPersonVisits(t *testing.T) {
	output := captureLogOutput(func() {
		exporter := NewExporter("stdout", nil)

		visits := VisitEntries{
			{Person{"Vanessa Gärtner", "Neuer Jungfernstieg 18 84155 Bodenkirchen"}, TimeRange{parseTime("2021-12-24-16:26:49"), parseTime("2021-12-24-17:02:37")}, "Pfarrkirche Unterobertalspitze"},
			{Person{"Vanessa Gärtner", "Neuer Jungfernstieg 18 84155 Bodenkirchen"}, TimeRange{parseTime("2021-12-24-17:20:32"), parseTime("2021-12-24-17:40:56")}, "Weihnachtsmarkt Unterobertalspitze"},
		}

		exporter.ExportPersonVisits(visits)
	})

	expected :=
		"Aufenthalte am 24.12.2021\n" +
			"Personendaten:\n" +
			"  Name: Vanessa Gärtner\n" +
			"  Adresse: Neuer Jungfernstieg 18 84155 Bodenkirchen\n" +
			"\n" +
			"* Pfarrkirche Unterobertalspitze von 24.12.2021 16:26:49 Uhr bis 24.12.2021 17:02:37 Uhr\n" +
			"* Weihnachtsmarkt Unterobertalspitze von 24.12.2021 17:20:32 Uhr bis 24.12.2021 17:40:56 Uhr\n"
	assert.Equal(t, expected, output)
}

// In diesem Test wird überprüft, dass die ausgegebenen Aufenthalte an einem Ort mit den erwarteten Werten übereinstimmen
func TestStdoutExporter_ExportLocationVisits(t *testing.T) {
	output := captureLogOutput(func() {
		exporter := NewExporter("stdout", nil)

		visits := VisitEntries{
			{Person{"Vanessa Gärtner", "Neuer Jungfernstieg 18 84155 Bodenkirchen"}, TimeRange{parseTime("2021-12-24-16:26:49"), parseTime("2021-12-24-17:02:37")}, "Pfarrkirche Unterobertalspitze"},
			{Person{"Lucas Pfeifer", "Eschenweg 23 07309 Saalfeld"}, TimeRange{parseTime("2021-12-24-16:20:32"), parseTime("2021-12-24-17:10:56")}, "Pfarrkirche Unterobertalspitze"},
		}

		exporter.ExportLocationVisits(visits)
	})

	expected :=
		"Aufenthalte am 24.12.2021 bei/in Pfarrkirche Unterobertalspitze\n" +
			"\n" +
			"* Vanessa Gärtner (Adresse: Neuer Jungfernstieg 18 84155 Bodenkirchen) von 24.12.2021 16:26:49 Uhr bis 24.12.2021 17:02:37 Uhr\n" +
			"* Lucas Pfeifer (Adresse: Eschenweg 23 07309 Saalfeld) von 24.12.2021 16:20:32 Uhr bis 24.12.2021 17:10:56 Uhr\n"
	assert.Equal(t, expected, output)
}

// In diesem Test wird überprüft, dass bei fehlenden Daten eine entsprechende Fehlermeldung ausgegeben wird.
func TestStdoutExporter_ExportEmptyPersonVisits(t *testing.T) {
	output := captureLogOutput(func() {
		exporter := NewExporter("stdout", nil)
		visits := VisitEntries{}
		exporter.ExportPersonVisits(visits)
	})

	expected := "Es konnten keine Aufenthalte zu dieser Person gefunden werden.\n"
	assert.Equal(t, expected, output)
}

// In diesem Test wird überprüft, dass bei fehlenden Daten eine entsprechende Fehlermeldung ausgegeben wird.
func TestStdoutExporter_ExportEmptyLocationVisits(t *testing.T) {
	output := captureLogOutput(func() {
		exporter := NewExporter("stdout", nil)
		visits := VisitEntries{}
		exporter.ExportLocationVisits(visits)
	})

	expected := "Es konnten keine Aufenthalte zu diesem Ort gefunden werden.\n"
	assert.Equal(t, expected, output)
}

// In diesem Test wird überprüft, dass die Gesamtdauer mehrerer VisitEntries korrekt akkumuliert wird
func TestAccumulateDuration(t *testing.T) {
	startTime := time.Now()
	switchTime := startTime.Add(5000)
	endTime := switchTime.Add(5000)

	entries := VisitEntries{
		VisitEntry{Person{"", ""}, TimeRange{startTime, switchTime}, ""},
		VisitEntry{Person{"", ""}, TimeRange{switchTime, endTime}, ""},
	}

	assert.Equal(t, time.Duration(10000), accumulateDuration(entries))
}

// In diesem Test wird überprüft, dass mehrere Visit-Entries korrekt anhand der Personen kategorisiert werden.
func TestCategorizeByPerson(t *testing.T) {
	p1 := Person{"Person1", ""}
	p2 := Person{"Person2", ""}

	entries1 := VisitEntries{
		VisitEntry{p1, TimeRange{}, "1"},
		VisitEntry{p1, TimeRange{}, "2"},
	}
	entries2 := VisitEntries{
		VisitEntry{p2, TimeRange{}, "4"},
	}
	entries := make(VisitEntries, 0, len(entries1)+len(entries2))
	entries = append(entries, entries1...)
	entries = append(entries, entries2...)

	categorized := categorizeByPerson(entries)
	assert.ElementsMatch(t, entries1, categorized[p1])
	assert.ElementsMatch(t, entries2, categorized[p2])
}

// Source: https://stackoverflow.com/questions/26804642/how-to-test-a-functions-output-stdout-stderr-in-unit-tests
// Diese Methode hilft dabei die Ausgabe auf den Log-Stream zu prüfen.
func captureLogOutput(f func()) string {
	var buf bytes.Buffer
	log.SetOutput(&buf)
	f()
	log.SetOutput(os.Stderr)

	// Rückgabe ohne den Timestamp, der von log.Print ergänzt wird
	return buf.String()[20:]
}

// Diese Methode hilft dabei die Ausgabe in eine temporäre Datei zu lesen.
// Die temporäre Datei wird nach dem Test auch wieder gelöscht.
func captureTestFileOutput(f func(file *os.File)) string {
	file, err := os.CreateTemp("", "")
	if err != nil {
		panic(err)
	}
	// Stacked defer calls -> executed in reverse order
	defer func() { _ = os.Remove(file.Name()) }()
	defer func() { _ = file.Close() }()

	f(file)

	readBytes, err := ioutil.ReadFile(file.Name())
	if err != nil {
		panic(err)
	}

	return string(readBytes)
}
