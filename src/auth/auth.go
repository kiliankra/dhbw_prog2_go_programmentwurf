/*
 * 2627585, 3110300, 2858031
 */

package auth

import (
	"errors"
	cfg "prog2_go_programmentwurf/src/config"
	"prog2_go_programmentwurf/src/journal"
	"prog2_go_programmentwurf/src/locations"
	"prog2_go_programmentwurf/src/token"
	"strings"
)

type UserTokenPayload struct {
	Name       string `json:"name"`
	Address    string `json:"address"`
	LocationId int64  `json:"locationId"`
}

// Generates a user token for the user with the given location, name and address. Returns error if
// name or address are empty or token could not be generated. Second return value (bool)
// indicates whether the given input is valid. Writes entry to journal if login was successful.
func Login(locationId int64, name, address string, config *cfg.Config) (string, bool, error) {
	// verify that name and address are not empty
	trimmedName := strings.TrimSpace(name)
	if trimmedName == "" {
		return "", false, errors.New("anmeldung nicht möglich, da der Name leer ist")
	}

	trimmedAddress := strings.TrimSpace(address)
	if trimmedAddress == "" {
		return "", false, errors.New("anmeldung nicht möglich, da die Adresse leer ist")
	}

	// check if location with given id exists
	location, err := (*config).GetLocationById(locationId)
	if err != nil {
		return "", false, err
	}

	// generate user token
	userTokenPayload := UserTokenPayload{
		Name:       trimmedName,
		Address:    trimmedAddress,
		LocationId: locationId}

	token, err := token.GenerateJwtToken(userTokenPayload, (*config).TokenSignatureSecret())

	// create journal entry
	if err == nil {
		j := journal.NewJournal(config)
		j.WriteCheckIn(location.Name, trimmedName, trimmedAddress)
	}

	return token, true, err
}

// Checks if the given user token is valid.
func Logout(userToken string, config *cfg.Config) error {
	payload, err := CheckUserToken(userToken, (*config).TokenSignatureSecret())

	// create journal entry
	if err == nil {
		j := journal.NewJournal(config)

		location, err := (*config).GetLocationById(payload.LocationId)
		if err != nil {
			return err
		}

		j.WriteCheckOut(location.Name, payload.Name, payload.Address)
	}

	return err
}

// Checks the given JWT token that is expected to be a login token. Given secret is used to
// verify token signature. Returns LoginTokenPayload that is extracted from the token or
// error if token is invalid or contains invalid payload.
func CheckLoginToken(loginToken, secret string) (locations.LoginTokenPayload, error) {
	payload := locations.LoginTokenPayload{}
	err := token.VerifyJwtToken(loginToken, secret, &payload)
	if err != nil {
		return locations.LoginTokenPayload{}, errors.New("anmeldetoken ist ungültig, " + err.Error())
	}
	if payload.CreatedAt == 0 || payload.ExpiresAt == 0 || payload.LocationId == 0 {
		return locations.LoginTokenPayload{}, errors.New("anmeldetoken enthält ungültige Nullwerte")
	}

	return payload, nil
}

// Checks the given JWT token that is expected to be a user token. Given secret is used to
// verify token signature. Returns UserTokenPayload that is extracted from the token or
// error if token is invalid or contains invalid payload.
func CheckUserToken(userToken, secret string) (UserTokenPayload, error) {
	payload := UserTokenPayload{}
	err := token.VerifyJwtToken(userToken, secret, &payload)
	if err != nil {
		return UserTokenPayload{}, errors.New("benutzertoken ist ungültig, " + err.Error())
	}
	if payload.Name == "" || payload.Address == "" || payload.LocationId == 0 {
		return UserTokenPayload{}, errors.New("benutzertoken enthält ungültige Nullwerte")
	}

	return payload, nil
}
