/*
 * 2627585, 3110300, 2858031
 */

package auth

import (
	"io/ioutil"
	"os"
	cfg "prog2_go_programmentwurf/src/config"
	"prog2_go_programmentwurf/src/locations"
	"testing"

	"github.com/stretchr/testify/assert"
)

// tokens generated with https://jwt.io
// secret that is used to generated all tokens for the following tests
func getTestConfig() cfg.Config {
	// create temp file for secret
	file, err := ioutil.TempFile("", "*.txt")
	if err != nil {
		panic("could not create temp file")
	}
	// Stacked defer calls -> executed in reverse order
	// File can be deleted after the config has been generated
	defer func() { _ = os.Remove(file.Name()) }()
	defer func() { _ = file.Close() }()

	secret := "mySecret"
	_, err = file.WriteString(secret)
	if err != nil {
		panic("could not write to file")
	}

	// create temp file for locations
	fileLocations, err := ioutil.TempFile("", "*.xml")
	if err != nil {
		panic("could not create temp file")
	}
	// Stacked defer calls -> executed in reverse order
	// File can be deleted after the config has been generated
	defer func() { _ = os.Remove(fileLocations.Name()) }()
	defer func() { _ = fileLocations.Close() }()

	_, err = fileLocations.WriteString(
		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
			"<locations>\n" +
			"    <location>\n" +
			"        <id>1</id>\n" +
			"        <name>DHBW Mosbach, B-Gebäude</name>\n" +
			"    </location>\n" +
			"</locations>",
	)
	if err != nil {
		panic("could not write to file")
	}

	args := make([]string, 4)
	args[0] = "-tokensecretpath"
	args[1] = file.Name()
	args[2] = "-locationxmlpath"
	args[3] = fileLocations.Name()

	var config cfg.Config = cfg.NewConfig(args)
	return config
}

var testConfig = getTestConfig()

// token payload:
// createdAt: 99900000000000 (year 5135)
// expiresAt: 99999999999999 (year 5138)
// locationId: 1
const validLoginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjcmVhdGVkQXQiOjk5OTAwMDAwMDAwMDAwLCJleHBpcmVzQXQiOjk5OTk5OTk5OTk5OTk5LCJsb2NhdGlvbklkIjoxfQ.7aOxLq6Trn4QudnjNo9V3_ggQlykznIA6cL_MDL1BkQ"

// payload:
// {
// 	"name": "Max Mustermann",
// 	"address": "Musterstraße 123, 12345 Musterstadt",
// 	"locationId": 1
// }
const validUserToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiTWF4IE11c3Rlcm1hbm4iLCJhZGRyZXNzIjoiTXVzdGVyc3RyYcOfZSAxMjMsIDEyMzQ1IE11c3RlcnN0YWR0IiwibG9jYXRpb25JZCI6MX0.UbHLrGyj4QVzmZY1UQ47gRZhllQOHmqhMHS97y7iXN4"

var loginTokensWithInvalidFormat = []string{
	"",
	// wrong token algorithm (HS512)
	"eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJjcmVhdGVkQXQiOjE2MzQyOTkxMzcsImV4cGlyZXNBdCI6MTYzNDI5OTUwMCwibG9jYXRpb25JZCI6MX0.-xvflcBugixunFY1Z317PHWlDHmQELLgWBnjsoGHO6zOuIG5pf2-t97zMWtZaWa4LvWQ1kzbfWbKwjZ6RcuKKw",
	// wrong token header typ (NOTJWT)
	"eyJhbGciOiJIUzI1NiIsInR5cCI6Ik5PVEpXVCJ9.eyJjcmVhdGVkQXQiOjE2MzQyOTkxMzcsImV4cGlyZXNBdCI6MTYzNDI5OTUwMCwibG9jYXRpb25JZCI6MX0.UJ8e-aBP5yo-LfsPQbibx7n04Ig4vE0Fcdpb-7_qtMM",
	// token payload changes / signature not valid
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjcmVhdGVkQXQiOjE2MzQyOTkxMzcsImV4cGlyZXNBdCI6MTYzNDI5OTUwMCwibG9jYXRpb25JZCI6MTIzfQ.GXjJOehoGTr5Do2KztP0GorhTFlTUxZ9PWKQsGtK0tA",
}

var loginTokensWithInvalidPayload = []string{
	// createdAt missing
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHBpcmVzQXQiOjE2MzQyOTk1MDAsImxvY2F0aW9uSWQiOjF9.C3aIkGB7_NhukbDppLwHFkU5-4QfwWRk3VHYfFEcj5M",
	// expiresAt missing
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjcmVhdGVkQXQiOjE2MzQyOTkxMzcsImxvY2F0aW9uSWQiOjF9.jaslY2I8TbeDkvzUi9GQh2-K40e422YWp291narmtAk",
	// locationId missing
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjcmVhdGVkQXQiOjE2MzQyOTkxMzcsImV4cGlyZXNBdCI6MTYzNDI5OTUwMH0.-ooija7MnftK8Y7KD4xBSc9uYQ8jMPNjxAx3gGAX2WE",
	// createdAt is string, should be number
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjcmVhdGVkQXQiOiIxNjM0Mjk5MTM3IiwiZXhwaXJlc0F0IjoxNjM0Mjk5NTAwLCJsb2NhdGlvbklkIjoxfQ.R1PVebaephgX_b1V8Ke5WOYHgox32ECgQa1yFrtY-PY",
	// expiresAt is string, should be number
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjcmVhdGVkQXQiOjE2MzQyOTkxMzcsImV4cGlyZXNBdCI6IjE2MzQyOTk1MDAiLCJsb2NhdGlvbklkIjoxfQ.vyeZuevB4MLUsA7H62APJxoifA6DwFxjTA-yqYzNwVM",
	// locationId is string, should be number
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjcmVhdGVkQXQiOjE2MzQyOTkxMzcsImV4cGlyZXNBdCI6MTYzNDI5OTUwMCwibG9jYXRpb25JZCI6IjEifQ.IksG-emTS_IA6ICR1CnMsIs1R1FcnnbIeASM14RBTjQ",
}

var userTokensWithInvalidFormat = []string{
	"",
	// wrong token algorithm (HS512)
	"eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiTWF4IE11c3Rlcm1hbm4iLCJhZGRyZXNzIjoiTXVzdGVyc3RyYcOfZSAxMjMsIDEyMzQ1IE11c3RlcnN0YWR0IiwibG9jYXRpb25JZCI6MX0.LKy_O6gdy0rnI2YDqgU4izQgpQhlV7eEP4xkn4ptXKf9VoAObhyWJnQnGzn5AqbvRTpLJUn8B1aanwYxDNPVIA",
	// wrong token header typ (NOTJWT)
	"eyJhbGciOiJIUzI1NiIsInR5cCI6Ik5PVEpXVCJ9.eyJuYW1lIjoiTWF4IE11c3Rlcm1hbm4iLCJhZGRyZXNzIjoiTXVzdGVyc3RyYcOfZSAxMjMsIDEyMzQ1IE11c3RlcnN0YWR0IiwibG9jYXRpb25JZCI6MX0.PHB3Cg5BTS36L5ChXp93hNMtvg-3NRMCjL85YoUlTTU",
	// token payload changes / signature not valid
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiTWF4IE11c3Rlcm1hbm4iLCJhZGRyZXNzIjoiTXVzdGVyc3RyYcOfZSAxMjMsIDEyMzQ1IE11c3RlcnN0YWR0IiwibG9jYXRpb25JZCI6MTIzfQ.UbHLrGyj4QVzmZY1UQ47gRZhllQOHmqhMHS97y7iXN4",
}

var userTokensWithInvalidPayload = []string{
	// name missing
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZGRyZXNzIjoiTXVzdGVyc3RyYcOfZSAxMjMsIDEyMzQ1IE11c3RlcnN0YWR0IiwibG9jYXRpb25JZCI6MX0.FOUCzOUC5Y1umhDHoQbL3FCnPZUT2DYAnRxzVYmfoFY",
	// address missing
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiTWF4IE11c3Rlcm1hbm4iLCJsb2NhdGlvbklkIjoxfQ.Rea040c1B4bf_OdU6bK-ExYgzO_2KmTHgfRKs8-qqQw",
	// locationId missing
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiTWF4IE11c3Rlcm1hbm4iLCJhZGRyZXNzIjoiTXVzdGVyc3RyYcOfZSAxMjMsIDEyMzQ1IE11c3RlcnN0YWR0In0.BK3Ro6W4DpTqNX0SAbTIlFb477KasDuSYofEZ86qi5c",
	// name is number, should be string
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoxMjMsImFkZHJlc3MiOiJNdXN0ZXJzdHJhw59lIDEyMywgMTIzNDUgTXVzdGVyc3RhZHQiLCJsb2NhdGlvbklkIjoxfQ.eOTLPyKgBnc1zWRYaSjYOCOOvZho71paWtxAriFcZ1M",
	// address is number, should be string
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiTWF4IE11c3Rlcm1hbm4iLCJhZGRyZXNzIjoxMjMsImxvY2F0aW9uSWQiOjF9.13K-GF3OkZ5XZpk87XOydwld8lfOYOvL7IBffC5ImlI",
	// locationId is string, should be number
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiTWF4IE11c3Rlcm1hbm4iLCJhZGRyZXNzIjoiTXVzdGVyc3RyYcOfZSAxMjMsIDEyMzQ1IE11c3RlcnN0YWR0IiwibG9jYXRpb25JZCI6IjEifQ.erzB9S1nfF_w3DR0FR5p8jpcQoXolqqx0LAgWnfKXn8",
}

func TestCheckLoginTokenFailWhenTokenInvalid(t *testing.T) {
	for _, token := range loginTokensWithInvalidFormat {
		_, err := CheckLoginToken(token, testConfig.TokenSignatureSecret())
		assert.NotNil(t, err)
	}
}

func TestCheckLoginTokenFailWhenTokenPayloadInvalid(t *testing.T) {
	for _, token := range loginTokensWithInvalidPayload {
		_, err := CheckLoginToken(token, testConfig.TokenSignatureSecret())
		assert.NotNil(t, err)
	}
}

func TestCheckLoginTokenForValidToken(t *testing.T) {
	loginPayload, err := CheckLoginToken(validLoginToken, testConfig.TokenSignatureSecret())
	assert.Nil(t, err)

	expectedPayload := locations.LoginTokenPayload{CreatedAt: 99900000000000, ExpiresAt: 99999999999999, LocationId: 1}
	assert.Equal(t, expectedPayload, loginPayload)
}

func TestLoginErrorWhenNameOrAddressEmpty(t *testing.T) {
	_, inputOk, err := Login(1, "   ", "Musterstr 123", &testConfig)
	assert.NotNil(t, err)
	assert.False(t, inputOk)

	_, inputOk, err = Login(1, "Max Mustermann", "  ", &testConfig)
	assert.NotNil(t, err)
	assert.False(t, inputOk)
}

func TestLoginErroWhenLocationIdInvalid(t *testing.T) {
	_, inputOk, err := Login(-1, "Max Mustermann", "Musterstraße 123, 12345 Musterstadt", &testConfig)
	assert.NotNil(t, err)
	assert.False(t, inputOk)
}

func TestLoginUserToken(t *testing.T) {
	userToken, inputOk, err := Login(1, "Max Mustermann", "Musterstraße 123, 12345 Musterstadt", &testConfig)
	assert.Nil(t, err)
	assert.True(t, inputOk)
	assert.Equal(t, validUserToken, userToken)
}

func TestCheckUserTokenFailWhenTokenInvalid(t *testing.T) {
	for _, token := range userTokensWithInvalidFormat {
		_, err := CheckUserToken(token, testConfig.TokenSignatureSecret())
		assert.NotNil(t, err)
	}
}

func TestCheckUserTokenFailWhenTokenPayloadInvalid(t *testing.T) {
	for _, token := range userTokensWithInvalidPayload {
		_, err := CheckUserToken(token, testConfig.TokenSignatureSecret())
		assert.NotNil(t, err)
	}
}

func TestCheckUserTokenForValidToken(t *testing.T) {
	loginPayload, err := CheckUserToken(validUserToken, testConfig.TokenSignatureSecret())
	assert.Nil(t, err)

	expectedPayload := UserTokenPayload{Name: "Max Mustermann", Address: "Musterstraße 123, 12345 Musterstadt", LocationId: 1}
	assert.Equal(t, expectedPayload, loginPayload)
}

func TestLogoutErrorWhenJwtInvalid(t *testing.T) {
	for _, token := range append(userTokensWithInvalidFormat, userTokensWithInvalidPayload...) {
		err := Logout(token, &testConfig)
		assert.NotNil(t, err)
	}
}

func TestLogoutWhenJwtValid(t *testing.T) {
	err := Logout(validUserToken, &testConfig)
	assert.Nil(t, err)
}
