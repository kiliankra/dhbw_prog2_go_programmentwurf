/*
 * 2627585, 3110300, 2858031
 */

package journal

import (
	"encoding/csv"
	"errors"
	"log"
	"os"
	"time"
)

type Entry struct {
	Timestamp     time.Time
	Mode          EntryMode
	Location      string
	Name, Address string
}

func ReadJournalEntries(path string) ([]Entry, error) {
	file, err := os.Open(path)
	defer func() { _ = file.Close() }()

	if err != nil {
		log.Println("ERROR: Die Journal-Datei konnte nicht geöffnet werden: ", err)
		return nil, err
	}

	csvReader := csv.NewReader(file)
	records, err := csvReader.ReadAll()

	if err != nil {
		log.Println("ERROR: Die Einträge der Journal-Datei konnten nicht gelesen werden: ", err)
		return nil, err
	}

	toReturn := make([]Entry, 0, len(records))
	for _, record := range records {
		if len(record) < 5 {
			log.Println("ERROR: Ein Eintrag im Journal war ungültig, da nicht genügend Elemente enthalten waren.")
			return nil, errors.New("read invalid line in journal")
		}

		toReturn = append(toReturn, Entry{
			Timestamp: parseTime(record[0]),
			Mode:      EntryMode(record[1]),
			Location:  record[2],
			Name:      record[3],
			Address:   record[4],
		})
	}
	return toReturn, nil
}

func parseTime(timestring string) time.Time {
	tm, err := time.Parse(journalTimeFormat, timestring)
	if err != nil {
		return time.UnixMicro(0)
	}
	return tm
}
