/*
 * 2627585, 3110300, 2858031
 */

package journal

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	cfg "prog2_go_programmentwurf/src/config"
	"regexp"
	"sync"
	"testing"
	"time"
)

// Prüft, dass die geöffnete Datei zum angegebenen Timestamp passt
func TestOpenFile(t *testing.T) {
	journal, tempDir := getTestJournal(t)
	testtime := time.Date(2021, 4, 24, 0, 0, 0, 0, time.Local)
	defer func() { _ = os.RemoveAll(tempDir) }()

	journalFile, err := journal.openFile(testtime, false)
	expectedPath := fmt.Sprintf("%v/2021_04_24.journal.log", tempDir)
	assert.Nil(t, err)

	if journalFile != nil {
		defer func() { _ = journalFile.Close() }()
		assert.Equal(t, expectedPath, journalFile.Name())
	}
}

// Prüft, dass eine neue Instanz von Config anlegen lässt.
func TestNewJournalImpl(t *testing.T) {
	var journal Journal
	journal, tempDir := getTestJournal(t)
	defer func() { _ = os.RemoveAll(tempDir) }()

	assert.NotNil(t, journal)
}

// Prüft, dass es zu einer Fehlermeldung kommt, wenn das Journalverzeichnis bereits existiert (z.B. weil es bereits eine Datei dieses Namens gibt)
func TestJournalDirExistsAsFile(t *testing.T) {
	file, err := ioutil.TempFile("", "*-journal")
	assert.NotNil(t, file)
	assert.Nil(t, err)
	// Cleanup mit mehreren defer-Aufrufen → Ausführung in umgekehrter Reihenfolge
	defer func() { _ = os.Remove(file.Name()) }()
	defer func() { _ = file.Close() }()

	args := make([]string, 2)
	args[0] = "-journalpath"
	args[1] = file.Name()

	var config = cfg.NewConfig(args)
	assert.Panics(t, func() {
		NewJournal(&config)
	})
}

// In diesem Test wird geprüft, dass CheckIns korrekt ins Journal geschrieben werden
func TestJournalImpl_WriteCheckIn(t *testing.T) {
	journal, tempDir := getTestJournal(t)
	defer func() { _ = os.RemoveAll(tempDir) }()
	curTime := time.Now()

	journal.WriteCheckIn("Elbphilharmonie Hamburg", "Frederik Braun", "Kehrwieder 2 20457 Hamburg - \"Speicherstadt\"")
	journal.WriteCheckIn("Elbphilharmonie Hamburg", "Gerrit Braun", "Kehrwieder 2 20457 Hamburg - \"Speicherstadt\"")

	journalFile, err := journal.openFile(curTime, false)
	assert.Nil(t, err)
	defer func() { _ = journalFile.Close() }()

	bytes, err := ioutil.ReadAll(journalFile)
	assert.Nil(t, err)

	// Prüfe, dass beide Zeilen korrekt geschrieben worden sind
	regexPattern := "\\d{4}(-\\d{2}){3}(:\\d{2}){2},CI,\"?Elbphilharmonie Hamburg\"?,\"?Frederik Braun\"?,\"Kehrwieder 2 20457 Hamburg - \"\"Speicherstadt\"\"\"\\n" +
		"\\d{4}(-\\d{2}){3}(:\\d{2}){2},CI,\"?Elbphilharmonie Hamburg\"?,\"?Gerrit Braun\"?,\"Kehrwieder 2 20457 Hamburg - \"\"Speicherstadt\"\"\"\\n"

	matches, err := regexp.Match(regexPattern, bytes)
	assert.Nil(t, err)
	assert.True(t, matches)
}

// In diesem Test wird geprüft, dass CheckOuts korrekt ins Journal geschrieben werden
func TestJournalImpl_WriteCheckOut(t *testing.T) {
	journal, tempDir := getTestJournal(t)
	defer func() { _ = os.RemoveAll(tempDir) }()
	curTime := time.Now()

	journal.WriteCheckOut("Elbphilharmonie Hamburg", "Frederik Braun", "Kehrwieder 2 20457 Hamburg - \"Speicherstadt\"")
	journal.WriteCheckOut("Elbphilharmonie Hamburg", "Gerrit Braun", "Kehrwieder 2 20457 Hamburg - \"Speicherstadt\"")

	journalFile, err := journal.openFile(curTime, false)
	assert.Nil(t, err)
	defer func() { _ = journalFile.Close() }()

	bytes, err := ioutil.ReadAll(journalFile)
	assert.Nil(t, err)

	// Prüfe, dass beide Zeilen korrekt geschrieben worden sind
	regexPattern := "\\d{4}(-\\d{2}){3}(:\\d{2}){2},CO,\"?Elbphilharmonie Hamburg\"?,\"?Frederik Braun\"?,\"Kehrwieder 2 20457 Hamburg - \"\"Speicherstadt\"\"\"\\n" +
		"\\d{4}(-\\d{2}){3}(:\\d{2}){2},CO,\"?Elbphilharmonie Hamburg\"?,\"?Gerrit Braun\"?,\"Kehrwieder 2 20457 Hamburg - \"\"Speicherstadt\"\"\"\\n"

	matches, err := regexp.Match(regexPattern, bytes)
	assert.Nil(t, err)
	assert.True(t, matches)
}

// In diesem Test wird geprüft, dass die Journaldatei auch bei asynchronen Zugriffen korrekt geschrieben wird.
// Hier werden aktuell noch Goroutinen verwendet, die alle auf dem gleichen Threadpool laufen, daher ist noch unklar, ob hier tatsächlich asynchrone Zugriffe stattfinden
func TestJournalImpl_WriteConcurrent(t *testing.T) {
	journal, tempDir := getTestJournal(t)
	defer func() { _ = os.RemoveAll(tempDir) }()
	curTime := time.Now()
	wg := sync.WaitGroup{}

	wg.Add(5)
	go writeRepeatedCheckInAndCheckOut(&wg, journal, "DHBW Mosbach, E-Gebäude", "Prof. Helmut Neemann", "Lohrtalweg 10, Mosbach")
	go writeRepeatedCheckInAndCheckOut(&wg, journal, "DHBW Mosbach, A-Gebäude", "Prof. Alexander Auch", "Lohrtalweg 10, Mosbach")
	go writeRepeatedCheckInAndCheckOut(&wg, journal, "DHBW Mosbach, A-Gebäude", "Prof. Dirk Saller", "Lohrtalweg 10, Mosbach")
	go writeRepeatedCheckInAndCheckOut(&wg, journal, "DHBW Mosbach, E-Gebäude", "Prof. Jörg Mielebacher", "Lohrtalweg 10, Mosbach")
	go writeRepeatedCheckInAndCheckOut(&wg, journal, "DHBW Mosbach, Mensa", "Rudi Testrudi", "Wegweg 42, Neckarelz")

	wg.Wait()
	journalFile, err := journal.openFile(curTime, false)
	assert.Nil(t, err)
	defer func() { _ = journalFile.Close() }()

	bytes, err := ioutil.ReadAll(journalFile)
	assert.Nil(t, err)

	// Check if the journal only contains valid lines (and not any fractured) (DATE CI|CO LOCATION NAME ADDRESS)
	matches, err := regexp.Match("(\\d{4}(-\\d{2}){3}(:\\d{2}){2},(CI|CO),\"?.+\"?,\"?.+\"?,\"?.+\"?\\n)*", bytes)
	assert.Nil(t, err)
	assert.True(t, matches)
}

func writeRepeatedCheckInAndCheckOut(wg *sync.WaitGroup, journal Journal, location string, name string, address string) {
	cicoCount := 100 // 100 CiCos pro Person, um eine Kollision unter normalen Umständen möglichst wahrscheinlich zu gestalten
	for i := 0; i < cicoCount; i++ {
		journal.WriteCheckIn(location, name, address)
		journal.WriteCheckOut(location, name, address)
	}
	wg.Done()
}

func getTestJournal(t *testing.T) (journal Journal, dir string) {
	dirName, err := ioutil.TempDir("", "*-journal")
	assert.NotNil(t, dirName)
	assert.Nil(t, err)

	args := make([]string, 2)
	args[0] = "-journalpath"
	args[1] = dirName

	var config = cfg.NewConfig(args)
	journ := NewJournal(&config)
	return journ, dirName
}
