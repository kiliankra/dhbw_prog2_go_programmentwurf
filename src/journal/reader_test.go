/*
 * 2627585, 3110300, 2858031
 */

package journal

import (
	"encoding/csv"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"testing"
	"time"
)

// In diesem Test wird geprüft, dass eine angegebene Journaldatei korrekt eingelesen wird
func TestReadJournalEntries(t *testing.T) {
	testJournal := createTestJournalFile(t, getTestSet())
	defer func() { _ = os.Remove(testJournal) }()

	entries, err := ReadJournalEntries(testJournal)
	assert.Nil(t, err)
	assert.Equal(t, getTestSet(), entries)
}

// In diesem Test wird geprüft, dass ein fehlerhafter Timestamp wie erwartet behandelt wird (Nämlich als Timestamp 0)
func TestReadJournalEntryWithInvalidTimestamp(t *testing.T) {
	file, err := ioutil.TempFile("", "*.journal.log")
	assert.Nil(t, err)
	// Cleanup mit mehreren defer-Aufrufen → Ausführung in umgekehrter Reihenfolge
	defer func() { _ = os.Remove(file.Name()) }()
	defer func() { _ = file.Close() }()

	writer := csv.NewWriter(file)
	// Schreibe fehlerhaften Eintrag (falsches Zeitformat)
	_ = writer.Write([]string{time.Now().Format(time.RFC850), string(EntryModeCheckIn), "Test", "Dummy", "Here, there and everywhere"})
	writer.Flush()

	entries, err := ReadJournalEntries(file.Name())
	assert.Nil(t, err)
	assert.Equal(t, 1, len(entries))
	assert.Equal(t, time.UnixMicro(0), entries[0].Timestamp)
}

// In diesem Test wird geprüft, dass der Aufruf ohne Rückgabe von Datensätzen endet, wenn die Journaldatei nicht existiert
func TestReaderFileNotAvailable(t *testing.T) {
	entries, err := ReadJournalEntries("thisfiledoesnotexist.journal.log")
	assert.Nil(t, entries)
	assert.NotNil(t, err)
}

// In diesem Test wird geprüft, dass der Aufruf ohne Rückgabe von Datensätzen endet, wenn die Journaldatei ungültig ist
func TestReaderFileNotValidContent(t *testing.T) {
	file, err := ioutil.TempFile("", "*.journal.log")
	assert.Nil(t, err)
	// Cleanup mit mehreren defer-Aufrufen → Ausführung in umgekehrter Reihenfolge
	defer func() { _ = os.Remove(file.Name()) }()
	defer func() { _ = file.Close() }()

	_, err = file.WriteString("this is not a valid journal so it should not be parsed.\n" +
		"Instead the parsing should fail and therefore a nil-value should be returned.")
	assert.Nil(t, err)

	entries, err := ReadJournalEntries(file.Name())
	assert.Nil(t, entries)
	assert.NotNil(t, err)
}

// In diesem Test wird geprüft, dass der Aufruf ohne Rückgabe von Datensätzen endet, wenn die Journaldatei keine gültige CSV-Datei ist
func TestReaderFileNotValidCsv(t *testing.T) {
	file, err := ioutil.TempFile("", "*.journal.log")
	assert.Nil(t, err)
	// Cleanup mit mehreren defer-Aufrufen → Ausführung in umgekehrter Reihenfolge
	defer func() { _ = os.Remove(file.Name()) }()
	defer func() { _ = file.Close() }()

	_, err = file.WriteString("this is not valid csv since it contains a singular quote \". Therefore it should not be read.\n" +
		"Instead the reading should fail and therefore a nil-value should be returned.")
	assert.Nil(t, err)

	entries, err := ReadJournalEntries(file.Name())
	assert.Nil(t, entries)
	assert.NotNil(t, err)
}

// Hilfsmethode zur Bereitstellung von Datensätzen, die einige Sonderfälle enthalten
func getTestSet() []Entry {
	return []Entry{
		{parseTime("2021-11-18-08:00:20"), EntryModeCheckIn, "DHBW Mosbach E-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-18-08:10:20"), EntryModeCheckIn, "DHBW Mosbach E-Gebäude", "Alexander Auch", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-18-08:12:20"), EntryModeCheckOut, "DHBW Mosbach E-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-18-08:15:20"), EntryModeCheckIn, "DHBW Mosbach E-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-18-08:16:20"), EntryModeCheckOut, "DHBW Mosbach E-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-18-08:16:20"), EntryModeCheckOut, "DHBW Mosbach E-Gebäude", "Alexander Auch", "Lohrtalweg 10 Mosbach"},
		{parseTime("2021-11-18-08:17:20"), EntryModeCheckIn, "DHBW Mosbach B-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},
		// Fehlerfall: Falsche Reihenfolge der Zeiten
		{parseTime("2021-11-18-08:14:20"), EntryModeCheckIn, "DHBW Mosbach E-Gebäude", "Alexander Auch", "Lohrtalweg 10 Mosbach"},
		// Fehlerfall: Check-In ohne Check-Out
		{parseTime("2021-11-18-08:21:20"), EntryModeCheckIn, "DHBW Mosbach A-Gebäude", "Dirk Saller", "Lohrtalweg 10 Mosbach"},
		// Edge-Case: Person mit gleichem Namen, aber anderer Addresse [wird als andere Person behandelt]
		{parseTime("2021-11-18-08:21:20"), EntryModeCheckIn, "DHBW Mosbach Mensa", "Dirk Saller", "Lohrtalweg 14 Neckarelz"},
	}
}

// Hilfsmethode zur Erstellung eines gültigen Testjournals
func createTestJournalFile(t *testing.T, testSet []Entry) string {
	file, err := ioutil.TempFile("", "*.journal.log")
	assert.Nil(t, err)
	defer func() { _ = file.Close() }()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for entry := range testSet {
		_ = writer.Write([]string{
			testSet[entry].Timestamp.Format(journalTimeFormat),
			string(testSet[entry].Mode),
			testSet[entry].Location,
			testSet[entry].Name,
			testSet[entry].Address,
		})
	}

	return file.Name()
}
