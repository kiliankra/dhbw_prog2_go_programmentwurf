/*
 * 2627585, 3110300, 2858031
 */

package journal

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	cfg "prog2_go_programmentwurf/src/config"
	"sync"
	"time"
)

var journalMutex sync.Mutex

type EntryMode string

const (
	EntryModeCheckIn  = EntryMode("CI")
	EntryModeCheckOut = EntryMode("CO")
)

const journalTimeFormat = "2006-01-02-15:04:05"

// Journal ist eine Schnittstelle, die die Dokumentation von An- und Abmeldungen an einem Ort ermöglicht.
// Es können beliebig viele Instanzen von Journal erzeugt werden, die alle synchronisiert auf die gleiche Datei zugreifen.
type Journal interface {
	WriteCheckIn(placename string, name string, address string)
	WriteCheckOut(placename string, name string, address string)

	openFile(time time.Time, writeOnly bool) (file *os.File, err error)
}

type journImpl struct {
	config *cfg.Config
}

func NewJournal(config *cfg.Config) Journal {
	err := os.MkdirAll((*config).JournalDirectoryPath(), os.ModeDir)
	if err != nil {
		log.Panicln("ERROR: Journalverzeichnis konnte nicht angelegt werden.")
		return nil
	}

	return &journImpl{config: config}
}

func (j *journImpl) WriteCheckIn(placename string, name string, address string) {
	// Bestimmung der Zeit bevor die synchronisierte Methode betreten wird
	curTime := time.Now()
	j.writeEntry(curTime, EntryModeCheckIn, placename, name, address)
}

func (j *journImpl) WriteCheckOut(placename string, name string, address string) {
	// Bestimmung der Zeit bevor die synchronisierte Methode betreten wird
	curTime := time.Now()
	j.writeEntry(curTime, EntryModeCheckOut, placename, name, address)
}

func (j *journImpl) writeEntry(entryTime time.Time, mode EntryMode, placename string, name string, address string) {
	// Synchronisiere auf den journalMutex während der Methode
	journalMutex.Lock()
	defer journalMutex.Unlock()
	f, _ := j.openFile(entryTime, true)
	defer func() { _ = f.Close() }()

	writer := csv.NewWriter(f)

	timestamp := entryTime.Format(journalTimeFormat)
	entry := []string{timestamp, string(mode), placename, name, address}
	_ = writer.Write(entry)
	writer.Flush()
}

func (j *journImpl) openFile(time time.Time, writeOnly bool) (file *os.File, err error) {
	year, month, day := time.Date()
	path := fmt.Sprintf("%v/%04d_%02d_%02d.journal.log",
		(*j.config).JournalDirectoryPath(),
		year, month, day)
	modeFlag := os.O_RDONLY
	if writeOnly {
		modeFlag = os.O_WRONLY
	}
	return os.OpenFile(path, os.O_APPEND|os.O_CREATE|modeFlag, 0660)
}
