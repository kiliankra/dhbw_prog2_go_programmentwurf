/*
 * 2627585, 3110300, 2858031
 */

package locations

import (
	"io/ioutil"
	"os"
	cfg "prog2_go_programmentwurf/src/config"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGenerateNewLoginToken(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.NoError(t, err)

	generatedLoginToken, err := GenerateNewLoginToken(&testConfig, 1)
	assert.NoError(t, err)
	assert.True(t, generatedLoginToken != "")
}

func TestGetLocationNameValidId(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.NoError(t, err)

	const testLocationId = 1
	const expectedLocationName = "DHBW Mosbach, A-Gebäude"
	actualLocation, err := testConfig.GetLocationById(testLocationId)
	assert.NoError(t, err)
	assert.NotNil(t, actualLocation)
	actualLocationName := actualLocation.Name
	assert.Equal(t, expectedLocationName, actualLocationName)
}

func TestGetLocationNameInvalidId(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.NoError(t, err)

	const testLocationId = 42
	const expectedLocationName = ""
	actualLocation, err := testConfig.GetLocationById(testLocationId)
	assert.Equal(t, "keine location mit der id 42 gefunden", err.Error())
	actualLocationName := actualLocation.Name
	assert.Equal(t, expectedLocationName, actualLocationName)
}

func TestGetLocationContentInvalidLocation(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.NoError(t, err)

	actualLocationContent, err := GetLocationContent(&testConfig, 42)
	assert.Error(t, err)
	assert.Equal(t, "keine location mit der id 42 gefunden", err.Error())
	assert.True(t, actualLocationContent.QrCode == "")
	assert.True(t, actualLocationContent.QrCodeValidityMs == 0)
	assert.True(t, actualLocationContent.LocationName == "")
}

func TestGetLocationContent(t *testing.T) {
	testConfig, err := getTestConfig()
	assert.NoError(t, err)

	actualLocationContent, err := GetLocationContent(&testConfig, 1)
	assert.NoError(t, err)
	assert.True(t, actualLocationContent.QrCode != "")
	assert.True(t, actualLocationContent.QrCodeValidityMs > 0)
	assert.True(t, actualLocationContent.LocationName != "")
}

func getTestConfig() (cfg.Config, error) {

	// Create temporary locations file
	fileLocations, err := ioutil.TempFile("", "*.xml")
	if err != nil {
		return nil, err
	}

	// Defer calls are stacked and will be executed in reverse order
	defer func() { _ = os.Remove(fileLocations.Name()) }()
	defer func() { _ = fileLocations.Close() }()

	_, err = fileLocations.WriteString(
		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
			"<locations>\n" +
			"    <location>\n" +
			"        <id>1</id>\n" +
			"        <name>DHBW Mosbach, A-Gebäude</name>\n" +
			"    </location>\n" +
			"</locations>",
	)
	if err != nil {
		return nil, err
	}

	// Create temporary file with TokenSignatureSecret
	fileSecret, err := ioutil.TempFile("", "*.txt")
	if err != nil {
		return nil, err
	}

	defer func() { _ = os.Remove(fileSecret.Name()) }()
	defer func() { _ = fileSecret.Close() }()

	secret := "secretFooBar"
	_, err = fileSecret.WriteString(secret)
	if err != nil {
		return nil, err
	}

	// Set the starting parameters
	args := make([]string, 6)
	args[0] = "-locationxmlpath"
	args[1] = fileLocations.Name()
	args[2] = "-qrvalidity"
	args[3] = "10000"
	args[4] = "-tokensecretpath"
	args[5] = fileSecret.Name()

	return cfg.NewConfig(args), nil
}
