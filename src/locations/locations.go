/*
 * 2627585, 3110300, 2858031
 */

package locations

import (
	"fmt"
	cfg "prog2_go_programmentwurf/src/config"
	"prog2_go_programmentwurf/src/qrcode"
	"prog2_go_programmentwurf/src/token"
	"time"
)

type LoginTokenPayload struct {
	CreatedAt  int64 `json:"createdAt"`
	ExpiresAt  int64 `json:"expiresAt"`
	LocationId int64 `json:"locationId"`
}

type LocationContent struct {
	QrCode           string
	QrCodeValidityMs int64
	LocationName     string
}

// Aggregates and returns all information about a location: QR-Code, validity time and location name
func GetLocationContent(cfg *cfg.Config, locationId int64) (LocationContent, error) {
	// Get location data to given locationId
	location, err := (*cfg).GetLocationById(locationId)
	if err != nil {
		return LocationContent{"", 0, ""}, err
	}

	// Login token generation
	loginToken, err := GenerateNewLoginToken(cfg, locationId)
	if err != nil {
		return LocationContent{"", 0, ""}, err
	}

	// Construct Login-Url and generate QR-Code
	port := (*cfg).LoginSitePort()
	url := fmt.Sprintf("https://%v:%v%v?token=%v", (*cfg).ServerHostAddress(), fmt.Sprint(port), (*cfg).LoginSitePath(), loginToken)
	generatedQrcode, err := qrcode.GenerateQrCode(url)
	if err != nil {
		return LocationContent{"", 0, ""}, err
	}

	return LocationContent{generatedQrcode, (*cfg).QrCodeValidityDurationMs(), location.Name}, nil
}

// Generates a new login token for the given location and current time
func GenerateNewLoginToken(cfg *cfg.Config, locationId int64) (string, error) {
	currentTimeInMilliseconds := time.Now().UnixMilli()
	expiresAt := currentTimeInMilliseconds + (*cfg).QrCodeValidityDurationMs()
	loginTokenPayload := LoginTokenPayload{currentTimeInMilliseconds, expiresAt, locationId}

	return token.GenerateJwtToken(loginTokenPayload, (*cfg).TokenSignatureSecret())
}
