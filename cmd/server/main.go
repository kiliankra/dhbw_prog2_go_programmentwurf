/*
 * 2627585, 3110300, 2858031
 */

package main

import (
	"os"
	cfg "prog2_go_programmentwurf/src/config"
	"prog2_go_programmentwurf/src/webserver/auth"
	"prog2_go_programmentwurf/src/webserver/locations"
)

func main() {
	var config = cfg.NewConfig(os.Args[1:])
	cfg.PrintConfig(config)

	go locations.Start(&config)
	auth.Start(&config)
}
