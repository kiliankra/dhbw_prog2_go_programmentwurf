/*
 * 2627585, 3110300, 2858031
 */

package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path"
	"prog2_go_programmentwurf/src/analysis"
	"prog2_go_programmentwurf/src/journal"
	"regexp"
	"strings"
)

func main() {
	// Call start with the arguments (but omit the called program name)
	start(os.Args[1:])
}

func start(args []string) {
	flags := flag.NewFlagSet("analyzer_args", flag.ContinueOnError)

	// Grundlegende Einstellungen
	input := flags.String("input", "journal.log", "Pfad zum Journal das analysiert werden soll. Falls die Datei nicht existiert oder nicht gelesen werden kann, ist keine Analyse möglich")
	basePath := flags.String("path", "exports/", "Pfad zum Verzeichnis, in dem die Ergebnisse der Analyse abgelegt werden")
	isCsv := flags.Bool("csv", false,
		"Ob die Ergebnisse der Analyse im CSV-Format exportiert werden sollen. "+
			"Wenn dieser Parameter `false` ist, werden die Ergebnisse nur in die Standardausgabe geschrieben")

	// Einstellungen für die jeweiligen Exportmethoden
	//   Aufenthaltsorte einer Person
	extractPersonLocations := flags.Bool("person_visits", false, "Ob die Aufenthaltsorte einer Person extrahiert werden sollen")
	extractPersonLocationsFile := flags.String("person_visits_file", "*_visits.csv",
		"Dateiname der verwendet wird um die exportierten Aufenthalte einer Person als CSV zu speichern. "+
			"Dies ist ein relativer Pfad zum Basisverzeichnis und kann nicht absolut angegeben werden (bei absoluten Pfaden wird versucht diese relativ zum Basisverzeichnis zu interpretieren)."+
			"Es kann ein Platzhalter '*' eingefügt werden. Dieser wird ersetzt, damit die Datei eindeutig einer Person zugeordnet werden kann.")

	//   Aufenthalte an einem Ort
	extractLocationVisits := flags.Bool("location_visits", false, "Ob die Anwesenheitsliste eines Ortes erstellt werden soll")
	extractLocationVisitsFile := flags.String("location_visits_file", "loc_*_visits.csv",
		"Dateiname der verwendet wird um die exportierten Besuche des Ortes als CSV zu speichern. "+
			"Dies ist ein relativer Pfad zum Basisverzeichnis und kann nicht absolut angegeben werden (bei absoluten Pfaden wird versucht diese relativ zum Basisverzeichnis zu interpretieren)."+
			"Es kann ein Platzhalter '*' eingefügt werden. Dieser wird ersetzt, damit die Datei eindeutig einem Ort zugeordnet werden kann.")

	//   Mögliche Kontakte einer Person
	extractContacts := flags.Bool("contacts", false, "Ob die Anwesenheitsliste eines Ortes erstellt werden soll")
	extractContactsFile := flags.String("contacts_file", "*_contacts.csv",
		"Dateiname der verwendet wird um die exportierten Kontakte einer Person als CSV zu speichern. "+
			"Dies ist ein relativer Pfad zum Basisverzeichnis und kann nicht absolut angegeben werden (bei absoluten Pfaden wird versucht diese relativ zum Basisverzeichnis zu interpretieren)."+
			"Es kann ein Platzhalter '*' eingefügt werden. Dieser wird ersetzt, damit die Datei eindeutig einer Person zugeordnet werden kann.")

	// Parameter zur Einschränkung der exportierten Daten
	extractPersonName := flags.String("person", ".*", "Regex für den Namen der Personen, deren Daten extrahiert werden sollen (nicht relevant für location_visits).")
	extractLocationName := flags.String("location", ".*", "Regex für den Namen der Orte, deren Daten extrahiert werden sollen (nur relevant für location_visits).")

	err := flags.Parse(args)
	if err != nil {
		log.Println("ERROR: Fehler bei der Auswertung der Parameter: ", err)
		os.Exit(-1)
	}

	// Auslesen des Journals und Erzeugung des Analyzers
	readEntries, err := journal.ReadJournalEntries(*input)
	if err != nil {
		log.Println("ERROR: Fehler beim Lesen der angegebenen JournalDatei: ", err)
		os.Exit(-2)
	}
	var analyzer = analysis.NewAnalyzer(readEntries)

	// Lese die regulären Ausdrücke zur Begrenzung von Person/Ort und erstelle ein Pattern daraus
	personRegex, err := regexp.Compile(*extractPersonName)
	if err != nil {
		log.Println("ERROR: Fehler bei der Auswertung des regulären Ausdrucks für den Personennamen: ", err)
		os.Exit(-3)
	}
	locationRegex, err := regexp.Compile(*extractLocationName)
	if err != nil {
		log.Println("ERROR: Fehler bei der Auswertung des regulären Ausdrucks für den Ortsnamen: ", err)
		os.Exit(-3)
	}

	if *extractPersonLocations {
		doExtractPersonVisits(&analyzer, personRegex, *basePath, *extractPersonLocationsFile, *isCsv)
	}
	if *extractLocationVisits {
		doExtractLocationVisits(&analyzer, locationRegex, *basePath, *extractLocationVisitsFile, *isCsv)
	}
	if *extractContacts {
		doExtractPersonContacts(&analyzer, personRegex, *basePath, *extractContactsFile, *isCsv)
	}
}

func doExtractPersonVisits(analyzer *analysis.Analyzer, personRegex *regexp.Regexp, outputBasePath string, outputFileTemplate string, isCsv bool) {
	people := (*analyzer).GetPeople()
	for _, person := range people {
		if personRegex.MatchString(person.Name) {
			f := getFileForPerson(outputBasePath, outputFileTemplate, isCsv, person)
			exporter := getExporter(isCsv, f)
			exporter.ExportPersonVisits((*analyzer).GetVisitsForPerson(person))
			_ = f.Close()
		}
	}
}

func doExtractLocationVisits(analyzer *analysis.Analyzer, locationRegex *regexp.Regexp, outputBasePath string, outputFileTemplate string, isCsv bool) {
	locations := (*analyzer).GetLocations()
	for _, location := range locations {
		if locationRegex.MatchString(location) {
			f := getFileForLocation(outputBasePath, outputFileTemplate, isCsv, location)
			exporter := getExporter(isCsv, f)
			exporter.ExportLocationVisits((*analyzer).GetVisitsForLocation(location))
			_ = f.Close()
		}
	}
}

func doExtractPersonContacts(analyzer *analysis.Analyzer, personRegex *regexp.Regexp, outputBasePath string, outputFileTemplate string, isCsv bool) {
	people := (*analyzer).GetPeople()
	for _, person := range people {
		if personRegex.MatchString(person.Name) {
			f := getFileForPerson(outputBasePath, outputFileTemplate, isCsv, person)
			exporter := getExporter(isCsv, f)
			exporter.ExportPersonContacts((*analyzer).GetContactsForPerson(person))
			_ = f.Close()
		}
	}
}

func getExporter(isCsv bool, f *os.File) analysis.Exporter {
	var exportType string
	if isCsv {
		exportType = "csv"
	} else {
		exportType = "stdout"
	}

	return analysis.NewExporter(exportType, f)
}

func getFileForPerson(basePath string, template string, needFile bool, person analysis.Person) *os.File {
	if !needFile {
		return nil
	}

	replacementString := fmt.Sprintf("%s_%s", person.Name, person.Address)
	replacementString = strings.ReplaceAll(replacementString, " ", "_")
	if len(replacementString) > 30 {
		runes := []rune(replacementString)
		replacementString = string(runes[:30])
	}

	return openFileForWriting(basePath, template, replacementString)
}

func getFileForLocation(basePath string, template string, needFile bool, location string) *os.File {
	if !needFile {
		return nil
	}

	replacementString := strings.ReplaceAll(location, " ", "_")
	if len(replacementString) > 30 {
		runes := []rune(replacementString)
		replacementString = string(runes[:30])
	}

	return openFileForWriting(basePath, template, replacementString)
}

func openFileForWriting(basePath string, template string, replacementString string) *os.File {
	postfix := strings.ReplaceAll(template, "*", replacementString)
	fullPath := path.Join(basePath, postfix)

	// Erstelle Verzeichnis
	err := os.MkdirAll(path.Dir(fullPath), os.ModeDir)
	if err != nil {
		log.Panicln("Fehler beim Anlegen des Zielverzeichnisses: ", err)
		return nil
	}

	file, err := os.OpenFile(fullPath, os.O_WRONLY|os.O_CREATE|os.O_APPEND, os.ModeAppend)
	if err != nil {
		log.Panicln("Fehler beim Anlegen der Exportdatei: ", err)
		return nil
	}
	return file
}
