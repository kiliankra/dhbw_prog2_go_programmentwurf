/*
 * 2627585, 3110300, 2858031
 */

package main

import (
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"path"
	"prog2_go_programmentwurf/src/analysis"
	"testing"
)

// Verschiedene Inhalte von Testdateien, die je nach Test geladen werden (oder verglichen werden können)

const testJournalContent = "" +
	"2021-11-18-10:14:38,CI,Alte Mälzerei Mosbach,Martina Fuchs,Hermannstrasse 5 68649 Groß-Rohrheim\n" +
	"2021-11-18-13:42:52,CO,Alte Mälzerei Mosbach,Martina Fuchs,Hermannstrasse 5 68649 Groß-Rohrheim\n" +
	"2021-11-18-17:44:30,CI,DHBW Mosbach E-Gebäude,Martina Fuchs,Hermannstrasse 5 68649 Groß-Rohrheim\n" +
	"2021-11-18-20:34:52,CO,DHBW Mosbach E-Gebäude,Martina Fuchs,Hermannstrasse 5 68649 Groß-Rohrheim\n" +
	"2021-11-18-03:11:20,CO,Alte Mälzerei Mosbach,Marie Schreiber,Koenigstrasse 72 96359 Steinbach\n" +
	"2021-11-18-12:04:20,CI,Alte Mälzerei Mosbach,Marie Schreiber,Koenigstrasse 72 96359 Steinbach\n" +
	"2021-11-18-12:34:20,CI,Alte Mälzerei Mosbach,Marie Schreiber,Koenigstrasse 72 96359 Steinbach\n" +
	"2021-11-18-14:26:49,CO,Alte Mälzerei Mosbach,Marie Schreiber,Koenigstrasse 72 96359 Steinbach\n" +
	"2021-11-18-14:27:49,CO,DHBW Mosbach A-Gebäude,Marie Schreiber,Koenigstrasse 72 96359 Steinbach\n"

const testPersonVisitsCsvContent = "Ort,CheckIn,CheckOut\n" +
	"Alte Mälzerei Mosbach,2021-11-18 10:14:38,2021-11-18 13:42:52\n" +
	"DHBW Mosbach E-Gebäude,2021-11-18 17:44:30,2021-11-18 20:34:52\n"

const testLocationVisitsCsvContent = "Name,Adresse,CheckIn,CheckOut\n" +
	"Martina Fuchs,Hermannstrasse 5 68649 Groß-Rohrheim,2021-11-18 17:44:30,2021-11-18 20:34:52\n"

const testContactCsvContent = "Name,Adresse,Ort,CheckIn,CheckOut\n" +
	"Martina Fuchs,Hermannstrasse 5 68649 Groß-Rohrheim,Alte Mälzerei Mosbach,2021-11-18 12:04:20,2021-11-18 13:42:52\n"

// Als Test wird ein Durchlauf gestartet, in dem alle Exporte zu einer CSV-Datei durchgeführt werden.
// Die Einzelfunktionen sind bereits durch einzelne Unittests geprüft, daher wird hier getestet, dass die Verknüpfung funktioniert.
func TestStart(t *testing.T) {
	tmpDir, err := ioutil.TempDir("", "")
	assert.Nil(t, err)

	testJournal, err := os.Create(path.Join(tmpDir, "journal.log"))
	assert.Nil(t, err)
	_, err = testJournal.WriteString(testJournalContent)
	assert.Nil(t, err)

	args := []string{
		"-input", testJournal.Name(),
		"-path", tmpDir,
		"-person_visits",
		"-location_visits",
		"-contacts",
		"-csv",
	}

	start(args)

	// Prüfe, dass die exportierten CSV-Dateien den Erwartungen entsprechen
	content, err := ioutil.ReadFile(path.Join(tmpDir, "Martina_Fuchs_Hermannstrasse_5_visits.csv"))
	assert.Nil(t, err)
	assert.Equal(t, testPersonVisitsCsvContent, string(content))

	content, err = ioutil.ReadFile(path.Join(tmpDir, "loc_DHBW_Mosbach_E-Gebäude_visits.csv"))
	assert.Nil(t, err)
	assert.Equal(t, testLocationVisitsCsvContent, string(content))

	content, err = ioutil.ReadFile(path.Join(tmpDir, "Marie_Schreiber_Koenigstrasse__contacts.csv"))
	assert.Nil(t, err)
	assert.Equal(t, testContactCsvContent, string(content))
}

// In diesem Test wird geprüft, dass getExporter sowohl einen CSV, als auch einen StdOut-Exporter korrekt erzeugen kann.
func TestGetExporter(t *testing.T) {
	assert.NotNil(t, getExporter(true, nil))
	assert.NotNil(t, getExporter(false, nil))
}

// In diesem Test wird geprüft, dass zu einem angegebenen Template für eine Person die korrekte Datei und der korrekte Dateiname erzeugt wird.
func TestGetFileForPerson(t *testing.T) {
	basePath := os.TempDir()
	person := analysis.Person{
		Name:    "Tester Testerei",
		Address: "Musterstraße Musterhausen",
	}

	f := getFileForPerson(basePath, "*_test.csv", true, person)
	// Cleanup mit mehreren defer-Aufrufen -> Ausführung in umgekehrter Reihenfolge
	defer func() { _ = os.Remove(f.Name()) }()
	defer func() { _ = f.Close() }()

	assert.NotNil(t, f)
	_, filename := path.Split(f.Name())
	assert.Equal(t, filename, "Tester_Testerei_Musterstraße_M_test.csv")

	// Prüfe, dass keine Datei erzeugt wird, wenn diese nicht benötigt wird.
	assert.Nil(t, getFileForPerson("", "", false, person))
}

// In diesem Test wird geprüft, dass zu einem angegebenen Template für einen Ort die korrekte Datei und der korrekte Dateiname erzeugt wird.
func TestGetFileForLocation(t *testing.T) {
	basePath := os.TempDir()
	location := "DHBW Mosbach Gebäude-E Eingang 2"

	f := getFileForLocation(basePath, "*_test.csv", true, location)
	// Cleanup mit mehreren defer-Aufrufen -> Ausführung in umgekehrter Reihenfolge
	defer func() { _ = os.Remove(f.Name()) }()
	defer func() { _ = f.Close() }()

	assert.NotNil(t, f)
	_, filename := path.Split(f.Name())
	assert.Equal(t, filename, "DHBW_Mosbach_Gebäude-E_Eingang_test.csv")

	// Prüfe, dass keine Datei erzeugt wird, wenn diese nicht benötigt wird.
	assert.Nil(t, getFileForLocation("", "", false, location))
}

// In diesem Test wird geprüft, dass die Methode openFileForWriting die Dateien korrekt öffnet und (falls nötig) erstellt
func TestOpenFileForWriting(t *testing.T) {
	testDir, err := ioutil.TempDir("", "")
	assert.Nil(t, err)
	defer func() { _ = os.RemoveAll(testDir) }()

	// Standardfall: Datei kann problemlos angelegt werden
	f := openFileForWriting(testDir, "*_*.*", "txt")
	assert.NotNil(t, f)
	assert.Equal(t, "txt_txt.txt", path.Base(f.Name()))
	_ = f.Close()

	// Fehlerfall: Elternverzeichnis kann nicht angelegt werden
	assert.Panics(t, func() {
		openFileForWriting(testDir, "txt_txt.txt/subfile", "")
	})

	// Fehlerfall: Datei kann nicht angelegt werden
	assert.Panics(t, func() {
		dir, file := path.Split(testDir)
		openFileForWriting(dir, file, "")
	})
}
