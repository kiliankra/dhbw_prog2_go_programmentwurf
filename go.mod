module prog2_go_programmentwurf

go 1.17

require github.com/stretchr/testify v1.7.0

// Automatisch durch GoLand eingefügt aufgrund transitiver Abhängigkeiten
require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)

require github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
